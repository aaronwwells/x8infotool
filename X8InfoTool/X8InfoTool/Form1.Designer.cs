﻿using System;

namespace X8InfoTool
{
    partial class X8InfoToolForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend7 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea8 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend8 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.OpenPortsBTN = new System.Windows.Forms.Button();
            this.GetCommPortsBTN = new System.Windows.Forms.Button();
            this.CommPortsCB = new System.Windows.Forms.ComboBox();
            this.CommPortsLBL = new System.Windows.Forms.Label();
            this.X8CommPortLBL = new System.Windows.Forms.Label();
            this.ClosePortBtn = new System.Windows.Forms.Button();
            this.EventTimer = new System.Windows.Forms.Timer(this.components);
            this.GetNumSavedMeasBTN = new System.Windows.Forms.Button();
            this.DisplaySavedMeasBTN = new System.Windows.Forms.Button();
            this.measurement_clb = new System.Windows.Forms.CheckedListBox();
            this.SelectAllBTN = new System.Windows.Forms.Button();
            this.SelectBetweenChecksBTN = new System.Windows.Forms.Button();
            this.ClearAllBTN = new System.Windows.Forms.Button();
            this.SaveToPCBTN = new System.Windows.Forms.Button();
            this.GetX8TypeBTN = new System.Windows.Forms.Button();
            this.GetDeviceIsPausedBTN = new System.Windows.Forms.Button();
            this.SpectrumChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Debug_ListBox = new System.Windows.Forms.ListBox();
            this.WaveformChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.InclSpectrumChkBox = new System.Windows.Forms.CheckBox();
            this.InclWaveformChkBox = new System.Windows.Forms.CheckBox();
            this.PrimaryOverall_TB = new System.Windows.Forms.TextBox();
            this.PrimaryOverall_LBL = new System.Windows.Forms.Label();
            this.SecondaryOverall_LBL = new System.Windows.Forms.Label();
            this.SecondaryOverall_TB = new System.Windows.Forms.TextBox();
            this.OverallsTimeoutChkBox = new System.Windows.Forms.CheckBox();
            this.JustLoadBTN = new System.Windows.Forms.Button();
            this.SaveMeasurementsBTN = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.SpectrumChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WaveformChart)).BeginInit();
            this.SuspendLayout();
            // 
            // OpenPortsBTN
            // 
            this.OpenPortsBTN.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OpenPortsBTN.Location = new System.Drawing.Point(432, 12);
            this.OpenPortsBTN.Name = "OpenPortsBTN";
            this.OpenPortsBTN.Size = new System.Drawing.Size(104, 23);
            this.OpenPortsBTN.TabIndex = 336;
            this.OpenPortsBTN.Text = "Open X8 Port";
            this.OpenPortsBTN.UseVisualStyleBackColor = true;
            this.OpenPortsBTN.Click += new System.EventHandler(this.OpenPortsBTN_Click);
            // 
            // GetCommPortsBTN
            // 
            this.GetCommPortsBTN.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GetCommPortsBTN.Location = new System.Drawing.Point(31, 12);
            this.GetCommPortsBTN.Name = "GetCommPortsBTN";
            this.GetCommPortsBTN.Size = new System.Drawing.Size(111, 23);
            this.GetCommPortsBTN.TabIndex = 337;
            this.GetCommPortsBTN.Text = "Get Comm Ports";
            this.GetCommPortsBTN.UseVisualStyleBackColor = true;
            this.GetCommPortsBTN.Click += new System.EventHandler(this.GetCommPortsBTN_Click);
            // 
            // CommPortsCB
            // 
            this.CommPortsCB.FormattingEnabled = true;
            this.CommPortsCB.Location = new System.Drawing.Point(209, 14);
            this.CommPortsCB.Name = "CommPortsCB";
            this.CommPortsCB.Size = new System.Drawing.Size(212, 28);
            this.CommPortsCB.TabIndex = 338;
            // 
            // CommPortsLBL
            // 
            this.CommPortsLBL.AutoSize = true;
            this.CommPortsLBL.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CommPortsLBL.Location = new System.Drawing.Point(145, 18);
            this.CommPortsLBL.Name = "CommPortsLBL";
            this.CommPortsLBL.Size = new System.Drawing.Size(63, 16);
            this.CommPortsLBL.TabIndex = 339;
            this.CommPortsLBL.Text = "Comm Port:";
            // 
            // X8CommPortLBL
            // 
            this.X8CommPortLBL.AutoSize = true;
            this.X8CommPortLBL.Enabled = false;
            this.X8CommPortLBL.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X8CommPortLBL.Location = new System.Drawing.Point(542, 18);
            this.X8CommPortLBL.Name = "X8CommPortLBL";
            this.X8CommPortLBL.Size = new System.Drawing.Size(76, 16);
            this.X8CommPortLBL.TabIndex = 340;
            this.X8CommPortLBL.Text = "X8 Comm Port";
            // 
            // ClosePortBtn
            // 
            this.ClosePortBtn.Enabled = false;
            this.ClosePortBtn.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClosePortBtn.Location = new System.Drawing.Point(623, 12);
            this.ClosePortBtn.Name = "ClosePortBtn";
            this.ClosePortBtn.Size = new System.Drawing.Size(104, 23);
            this.ClosePortBtn.TabIndex = 341;
            this.ClosePortBtn.Text = "Close X8 Port";
            this.ClosePortBtn.UseVisualStyleBackColor = true;
            this.ClosePortBtn.Click += new System.EventHandler(this.ClosePortBtn_Click);
            // 
            // EventTimer
            // 
            this.EventTimer.Enabled = true;
            this.EventTimer.Interval = 1;
            this.EventTimer.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // GetNumSavedMeasBTN
            // 
            this.GetNumSavedMeasBTN.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GetNumSavedMeasBTN.Location = new System.Drawing.Point(31, 71);
            this.GetNumSavedMeasBTN.Name = "GetNumSavedMeasBTN";
            this.GetNumSavedMeasBTN.Size = new System.Drawing.Size(236, 29);
            this.GetNumSavedMeasBTN.TabIndex = 342;
            this.GetNumSavedMeasBTN.Text = "Get Number of Saved Measurements";
            this.GetNumSavedMeasBTN.UseVisualStyleBackColor = true;
            this.GetNumSavedMeasBTN.Click += new System.EventHandler(this.GetNumSavedMeasBTN_Click);
            // 
            // DisplaySavedMeasBTN
            // 
            this.DisplaySavedMeasBTN.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DisplaySavedMeasBTN.Location = new System.Drawing.Point(299, 71);
            this.DisplaySavedMeasBTN.Name = "DisplaySavedMeasBTN";
            this.DisplaySavedMeasBTN.Size = new System.Drawing.Size(183, 29);
            this.DisplaySavedMeasBTN.TabIndex = 343;
            this.DisplaySavedMeasBTN.Text = "Display Saved Measurements";
            this.DisplaySavedMeasBTN.UseVisualStyleBackColor = true;
            this.DisplaySavedMeasBTN.Click += new System.EventHandler(this.DisplaySavedMeasBTN_Click);
            // 
            // measurement_clb
            // 
            this.measurement_clb.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.measurement_clb.FormattingEnabled = true;
            this.measurement_clb.Location = new System.Drawing.Point(31, 156);
            this.measurement_clb.Name = "measurement_clb";
            this.measurement_clb.Size = new System.Drawing.Size(573, 174);
            this.measurement_clb.TabIndex = 344;
            // 
            // SelectAllBTN
            // 
            this.SelectAllBTN.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelectAllBTN.Location = new System.Drawing.Point(31, 126);
            this.SelectAllBTN.Name = "SelectAllBTN";
            this.SelectAllBTN.Size = new System.Drawing.Size(75, 23);
            this.SelectAllBTN.TabIndex = 345;
            this.SelectAllBTN.Text = "Select All";
            this.SelectAllBTN.UseVisualStyleBackColor = true;
            this.SelectAllBTN.Click += new System.EventHandler(this.SelectAllBTN_Click);
            // 
            // SelectBetweenChecksBTN
            // 
            this.SelectBetweenChecksBTN.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelectBetweenChecksBTN.Location = new System.Drawing.Point(248, 126);
            this.SelectBetweenChecksBTN.Name = "SelectBetweenChecksBTN";
            this.SelectBetweenChecksBTN.Size = new System.Drawing.Size(175, 23);
            this.SelectBetweenChecksBTN.TabIndex = 346;
            this.SelectBetweenChecksBTN.Text = "Select All Between Checked";
            this.SelectBetweenChecksBTN.UseVisualStyleBackColor = true;
            this.SelectBetweenChecksBTN.Click += new System.EventHandler(this.SelectAllBetweenCheckedBTN_Click);
            // 
            // ClearAllBTN
            // 
            this.ClearAllBTN.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClearAllBTN.Location = new System.Drawing.Point(134, 127);
            this.ClearAllBTN.Name = "ClearAllBTN";
            this.ClearAllBTN.Size = new System.Drawing.Size(75, 23);
            this.ClearAllBTN.TabIndex = 348;
            this.ClearAllBTN.Text = "Clear All";
            this.ClearAllBTN.UseVisualStyleBackColor = true;
            this.ClearAllBTN.Click += new System.EventHandler(this.ClearAllBTN_Click);
            // 
            // SaveToPCBTN
            // 
            this.SaveToPCBTN.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveToPCBTN.Location = new System.Drawing.Point(453, 127);
            this.SaveToPCBTN.Name = "SaveToPCBTN";
            this.SaveToPCBTN.Size = new System.Drawing.Size(178, 23);
            this.SaveToPCBTN.TabIndex = 349;
            this.SaveToPCBTN.Text = "Save Selected To PC";
            this.SaveToPCBTN.UseVisualStyleBackColor = true;
            this.SaveToPCBTN.Click += new System.EventHandler(this.SaveToPCBTN_Click);
            // 
            // GetX8TypeBTN
            // 
            this.GetX8TypeBTN.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GetX8TypeBTN.Location = new System.Drawing.Point(501, 71);
            this.GetX8TypeBTN.Name = "GetX8TypeBTN";
            this.GetX8TypeBTN.Size = new System.Drawing.Size(87, 29);
            this.GetX8TypeBTN.TabIndex = 350;
            this.GetX8TypeBTN.Text = "Get X8 Type";
            this.GetX8TypeBTN.UseVisualStyleBackColor = true;
            this.GetX8TypeBTN.Click += new System.EventHandler(this.GetX8TypeBTN_Click);
            // 
            // GetDeviceIsPausedBTN
            // 
            this.GetDeviceIsPausedBTN.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GetDeviceIsPausedBTN.Location = new System.Drawing.Point(609, 71);
            this.GetDeviceIsPausedBTN.Name = "GetDeviceIsPausedBTN";
            this.GetDeviceIsPausedBTN.Size = new System.Drawing.Size(129, 29);
            this.GetDeviceIsPausedBTN.TabIndex = 351;
            this.GetDeviceIsPausedBTN.Text = "Get Device is Paused";
            this.GetDeviceIsPausedBTN.UseVisualStyleBackColor = true;
            this.GetDeviceIsPausedBTN.Click += new System.EventHandler(this.GetDeviceIsPausedBTN_Click);
            // 
            // SpectrumChart
            // 
            chartArea7.Name = "ChartArea1";
            this.SpectrumChart.ChartAreas.Add(chartArea7);
            legend7.Name = "Legend1";
            this.SpectrumChart.Legends.Add(legend7);
            this.SpectrumChart.Location = new System.Drawing.Point(610, 156);
            this.SpectrumChart.Name = "SpectrumChart";
            series7.ChartArea = "ChartArea1";
            series7.Legend = "Legend1";
            series7.Name = "Series1";
            this.SpectrumChart.Series.Add(series7);
            this.SpectrumChart.Size = new System.Drawing.Size(660, 319);
            this.SpectrumChart.TabIndex = 352;
            this.SpectrumChart.Text = "chart1";
            // 
            // Debug_ListBox
            // 
            this.Debug_ListBox.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Debug_ListBox.FormattingEnabled = true;
            this.Debug_ListBox.ItemHeight = 16;
            this.Debug_ListBox.Location = new System.Drawing.Point(31, 346);
            this.Debug_ListBox.Name = "Debug_ListBox";
            this.Debug_ListBox.Size = new System.Drawing.Size(573, 452);
            this.Debug_ListBox.TabIndex = 353;
            // 
            // WaveformChart
            // 
            chartArea8.Name = "ChartArea1";
            this.WaveformChart.ChartAreas.Add(chartArea8);
            legend8.Name = "Legend1";
            this.WaveformChart.Legends.Add(legend8);
            this.WaveformChart.Location = new System.Drawing.Point(610, 486);
            this.WaveformChart.Name = "WaveformChart";
            series8.ChartArea = "ChartArea1";
            series8.Legend = "Legend1";
            series8.Name = "Series1";
            this.WaveformChart.Series.Add(series8);
            this.WaveformChart.Size = new System.Drawing.Size(660, 319);
            this.WaveformChart.TabIndex = 354;
            this.WaveformChart.Text = "chart1";
            // 
            // InclSpectrumChkBox
            // 
            this.InclSpectrumChkBox.AutoSize = true;
            this.InclSpectrumChkBox.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InclSpectrumChkBox.Location = new System.Drawing.Point(646, 126);
            this.InclSpectrumChkBox.Name = "InclSpectrumChkBox";
            this.InclSpectrumChkBox.Size = new System.Drawing.Size(108, 20);
            this.InclSpectrumChkBox.TabIndex = 355;
            this.InclSpectrumChkBox.Text = "Include Spectrum";
            this.InclSpectrumChkBox.UseVisualStyleBackColor = true;
            // 
            // InclWaveformChkBox
            // 
            this.InclWaveformChkBox.AutoSize = true;
            this.InclWaveformChkBox.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InclWaveformChkBox.Location = new System.Drawing.Point(776, 126);
            this.InclWaveformChkBox.Name = "InclWaveformChkBox";
            this.InclWaveformChkBox.Size = new System.Drawing.Size(111, 20);
            this.InclWaveformChkBox.TabIndex = 356;
            this.InclWaveformChkBox.Text = "Include Waveform";
            this.InclWaveformChkBox.UseVisualStyleBackColor = true;
            // 
            // PrimaryOverall_TB
            // 
            this.PrimaryOverall_TB.Location = new System.Drawing.Point(886, 14);
            this.PrimaryOverall_TB.Name = "PrimaryOverall_TB";
            this.PrimaryOverall_TB.Size = new System.Drawing.Size(100, 26);
            this.PrimaryOverall_TB.TabIndex = 357;
            this.PrimaryOverall_TB.TextChanged += new System.EventHandler(this.OverallA_TB_TextChanged);
            // 
            // PrimaryOverall_LBL
            // 
            this.PrimaryOverall_LBL.AutoSize = true;
            this.PrimaryOverall_LBL.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrimaryOverall_LBL.Location = new System.Drawing.Point(768, 16);
            this.PrimaryOverall_LBL.Name = "PrimaryOverall_LBL";
            this.PrimaryOverall_LBL.Size = new System.Drawing.Size(52, 16);
            this.PrimaryOverall_LBL.TabIndex = 358;
            this.PrimaryOverall_LBL.Text = "OverallA:";
            // 
            // SecondaryOverall_LBL
            // 
            this.SecondaryOverall_LBL.AutoSize = true;
            this.SecondaryOverall_LBL.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SecondaryOverall_LBL.Location = new System.Drawing.Point(768, 42);
            this.SecondaryOverall_LBL.Name = "SecondaryOverall_LBL";
            this.SecondaryOverall_LBL.Size = new System.Drawing.Size(52, 16);
            this.SecondaryOverall_LBL.TabIndex = 360;
            this.SecondaryOverall_LBL.Text = "OverallB:";
            // 
            // SecondaryOverall_TB
            // 
            this.SecondaryOverall_TB.Location = new System.Drawing.Point(886, 40);
            this.SecondaryOverall_TB.Name = "SecondaryOverall_TB";
            this.SecondaryOverall_TB.Size = new System.Drawing.Size(100, 26);
            this.SecondaryOverall_TB.TabIndex = 359;
            // 
            // OverallsTimeoutChkBox
            // 
            this.OverallsTimeoutChkBox.AutoSize = true;
            this.OverallsTimeoutChkBox.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OverallsTimeoutChkBox.Location = new System.Drawing.Point(1014, 17);
            this.OverallsTimeoutChkBox.Name = "OverallsTimeoutChkBox";
            this.OverallsTimeoutChkBox.Size = new System.Drawing.Size(106, 20);
            this.OverallsTimeoutChkBox.TabIndex = 361;
            this.OverallsTimeoutChkBox.Text = "Overalls Timeout";
            this.OverallsTimeoutChkBox.UseVisualStyleBackColor = true;
            // 
            // JustLoadBTN
            // 
            this.JustLoadBTN.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JustLoadBTN.Location = new System.Drawing.Point(910, 123);
            this.JustLoadBTN.Name = "JustLoadBTN";
            this.JustLoadBTN.Size = new System.Drawing.Size(75, 23);
            this.JustLoadBTN.TabIndex = 362;
            this.JustLoadBTN.Text = "Just Load";
            this.JustLoadBTN.UseVisualStyleBackColor = true;
            this.JustLoadBTN.Click += new System.EventHandler(this.JustLoadBTN_Click);
            // 
            // SaveMeasurementsBTN
            // 
            this.SaveMeasurementsBTN.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveMeasurementsBTN.Location = new System.Drawing.Point(1006, 123);
            this.SaveMeasurementsBTN.Name = "SaveMeasurementsBTN";
            this.SaveMeasurementsBTN.Size = new System.Drawing.Size(135, 23);
            this.SaveMeasurementsBTN.TabIndex = 363;
            this.SaveMeasurementsBTN.Text = "Save Measurements";
            this.SaveMeasurementsBTN.UseVisualStyleBackColor = true;
            this.SaveMeasurementsBTN.Click += new System.EventHandler(this.SaveMeasurementsBTN_Click);
            // 
            // X8InfoToolForm
            // 
            this.ClientSize = new System.Drawing.Size(1292, 824);
            this.Controls.Add(this.SaveMeasurementsBTN);
            this.Controls.Add(this.JustLoadBTN);
            this.Controls.Add(this.OverallsTimeoutChkBox);
            this.Controls.Add(this.SecondaryOverall_LBL);
            this.Controls.Add(this.SecondaryOverall_TB);
            this.Controls.Add(this.PrimaryOverall_LBL);
            this.Controls.Add(this.PrimaryOverall_TB);
            this.Controls.Add(this.InclWaveformChkBox);
            this.Controls.Add(this.InclSpectrumChkBox);
            this.Controls.Add(this.WaveformChart);
            this.Controls.Add(this.Debug_ListBox);
            this.Controls.Add(this.SpectrumChart);
            this.Controls.Add(this.GetDeviceIsPausedBTN);
            this.Controls.Add(this.GetX8TypeBTN);
            this.Controls.Add(this.SaveToPCBTN);
            this.Controls.Add(this.ClearAllBTN);
            this.Controls.Add(this.SelectBetweenChecksBTN);
            this.Controls.Add(this.SelectAllBTN);
            this.Controls.Add(this.measurement_clb);
            this.Controls.Add(this.DisplaySavedMeasBTN);
            this.Controls.Add(this.GetNumSavedMeasBTN);
            this.Controls.Add(this.ClosePortBtn);
            this.Controls.Add(this.X8CommPortLBL);
            this.Controls.Add(this.CommPortsLBL);
            this.Controls.Add(this.CommPortsCB);
            this.Controls.Add(this.GetCommPortsBTN);
            this.Controls.Add(this.OpenPortsBTN);
            this.Name = "X8InfoToolForm";
            this.Text = "X8 Info Tool V0.0.0.1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SpectrumChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WaveformChart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    //private void Ch8RPMFloor_TB_TextChanged(object sender, EventArgs e)
    //{
    //    throw new NotImplementedException();
    //}

    #endregion
            /*
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox IP3TB;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.TextBox IP1TB;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.TextBox IP0TB;
    private System.Windows.Forms.TextBox IP2TB;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.TextBox FTPUSERTB;
    private System.Windows.Forms.TextBox FTPPWTB;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.TextBox FTPDIRTB;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.DateTimePicker RTCUpdateDateTimePicker;
    private System.Windows.Forms.CheckBox ForceRTCFlagCB;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.TextBox PlantTB;
    private System.Windows.Forms.TextBox TrainTB;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.TextBox MachineTB;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.Label label17;
    private System.Windows.Forms.CheckBox UseCh1CB;
    private System.Windows.Forms.CheckBox UseCh2CB;
    private System.Windows.Forms.CheckBox UseCh4CB;
    private System.Windows.Forms.CheckBox UseCh3CB;
    private System.Windows.Forms.CheckBox UseCh8CB;
    private System.Windows.Forms.CheckBox UseCh7CB;
    private System.Windows.Forms.CheckBox UseCh6CB;
    private System.Windows.Forms.CheckBox UseCh5CB;
    private System.Windows.Forms.TextBox LOC1TB;
    private System.Windows.Forms.TextBox LOC2TB;
    private System.Windows.Forms.TextBox LOC4TB;
    private System.Windows.Forms.TextBox LOC3TB;
    private System.Windows.Forms.TextBox LOC8TB;
    private System.Windows.Forms.TextBox LOC7TB;
    private System.Windows.Forms.TextBox LOC6TB;
    private System.Windows.Forms.TextBox LOC5TB;
    private System.Windows.Forms.TextBox COLL8TB;
    private System.Windows.Forms.TextBox COLL7TB;
    private System.Windows.Forms.TextBox COLL6TB;
    private System.Windows.Forms.TextBox COLL5TB;
    private System.Windows.Forms.TextBox COLL4TB;
    private System.Windows.Forms.TextBox COLL3TB;
    private System.Windows.Forms.TextBox COLL2TB;
    private System.Windows.Forms.TextBox COLL1TB;
    private System.Windows.Forms.Label Collections1LBL;
    private System.Windows.Forms.Label label20;
    private System.Windows.Forms.Label label21;
    private System.Windows.Forms.ComboBox Ch1SendRateCB;
    private System.Windows.Forms.ComboBox Ch2SendRateCB;
    private System.Windows.Forms.ComboBox Ch4SendRateCB;
    private System.Windows.Forms.ComboBox Ch3SendRateCB;
    private System.Windows.Forms.ComboBox Ch8SendRateCB;
    private System.Windows.Forms.ComboBox Ch7SendRateCB;
    private System.Windows.Forms.ComboBox Ch6SendRateCB;
    private System.Windows.Forms.ComboBox Ch5SendRateCB;
    private System.Windows.Forms.ComboBox Ch8TransTypeCB;
    private System.Windows.Forms.ComboBox Ch7TransTypeCB;
    private System.Windows.Forms.ComboBox Ch6TransTypeCB;
    private System.Windows.Forms.ComboBox Ch5TransTypeCB;
    private System.Windows.Forms.ComboBox Ch4TransTypeCB;
    private System.Windows.Forms.ComboBox Ch3TransTypeCB;
    private System.Windows.Forms.ComboBox Ch2TransTypeCB;
    private System.Windows.Forms.ComboBox Ch1TransTypeCB;
    private System.Windows.Forms.Label label22;
    private System.Windows.Forms.Label label23;
    private System.Windows.Forms.ComboBox Ch1SensCB;
    private System.Windows.Forms.Label label24;
    private System.Windows.Forms.ComboBox Ch2SensCB;
    private System.Windows.Forms.ComboBox Ch4SensCB;
    private System.Windows.Forms.ComboBox Ch3SensCB;
    private System.Windows.Forms.ComboBox Ch8SensCB;
    private System.Windows.Forms.ComboBox Ch7SensCB;
    private System.Windows.Forms.ComboBox Ch6SensCB;
    private System.Windows.Forms.ComboBox Ch5SensCB;
    private System.Windows.Forms.ComboBox Ch8HPFiltCB;
    private System.Windows.Forms.ComboBox Ch7HPFiltCB;
    private System.Windows.Forms.ComboBox Ch6HPFiltCB;
    private System.Windows.Forms.ComboBox Ch5HPFiltCB;
    private System.Windows.Forms.ComboBox Ch4HPFiltCB;
    private System.Windows.Forms.ComboBox Ch3HPFiltCB;
    private System.Windows.Forms.ComboBox Ch2HPFiltCB;
    private System.Windows.Forms.ComboBox Ch1HPFiltCB;
    private System.Windows.Forms.Label label25;
    private System.Windows.Forms.ComboBox TimeZoneCB;
    private System.Windows.Forms.Label label26;
    private System.Windows.Forms.ComboBox Ch8SigFormatCB;
    private System.Windows.Forms.ComboBox Ch7SigFormatCB;
    private System.Windows.Forms.ComboBox Ch6SigFormatCB;
    private System.Windows.Forms.ComboBox Ch5SigFormatCB;
    private System.Windows.Forms.ComboBox Ch4SigFormatCB;
    private System.Windows.Forms.ComboBox Ch3SigFormatCB;
    private System.Windows.Forms.ComboBox Ch2SigFormatCB;
    private System.Windows.Forms.ComboBox Ch1SigFormatCB;
    private System.Windows.Forms.Label label27;
    private System.Windows.Forms.ComboBox Ch8FMaxCB;
    private System.Windows.Forms.ComboBox Ch7FMaxCB;
    private System.Windows.Forms.ComboBox Ch6FMaxCB;
    private System.Windows.Forms.ComboBox Ch5FMaxCB;
    private System.Windows.Forms.ComboBox Ch4FMaxCB;
    private System.Windows.Forms.ComboBox Ch3FMaxCB;
    private System.Windows.Forms.ComboBox Ch2FMaxCB;
    private System.Windows.Forms.ComboBox Ch1FMaxCB;
    private System.Windows.Forms.Label label28;
    private System.Windows.Forms.ComboBox Ch8LORCB;
    private System.Windows.Forms.ComboBox Ch7LORCB;
    private System.Windows.Forms.ComboBox Ch6LORCB;
    private System.Windows.Forms.ComboBox Ch5LORCB;
    private System.Windows.Forms.ComboBox Ch4LORCB;
    private System.Windows.Forms.ComboBox Ch3LORCB;
    private System.Windows.Forms.ComboBox Ch2LORCB;
    private System.Windows.Forms.ComboBox Ch1LORCB;
    private System.Windows.Forms.Label label29;
    private System.Windows.Forms.ComboBox Ch8SpeedTrigCB;
    private System.Windows.Forms.ComboBox Ch7SpeedTrigCB;
    private System.Windows.Forms.ComboBox Ch6SpeedTrigCB;
    private System.Windows.Forms.ComboBox Ch5SpeedTrigCB;
    private System.Windows.Forms.ComboBox Ch4SpeedTrigCB;
    private System.Windows.Forms.ComboBox Ch3SpeedTrigCB;
    private System.Windows.Forms.ComboBox Ch2SpeedTrigCB;
    private System.Windows.Forms.ComboBox Ch1SpeedTrigCB;
    private System.Windows.Forms.Label label30;
    private System.Windows.Forms.Label label31;
    private System.Windows.Forms.TextBox Ch1PPRTB;
    private System.Windows.Forms.TextBox Ch2PPRTB;
    private System.Windows.Forms.TextBox Ch4PPRTB;
    private System.Windows.Forms.TextBox Ch3PPRTB;
    private System.Windows.Forms.TextBox Ch8PPRTB;
    private System.Windows.Forms.TextBox Ch7PPRTB;
    private System.Windows.Forms.TextBox Ch6PPRTB;
    private System.Windows.Forms.TextBox Ch5PPRTB;
    private System.Windows.Forms.Label label32;
    private System.Windows.Forms.Label label33;
    private System.Windows.Forms.CheckBox Ch1VelOvAllCB;
    private System.Windows.Forms.CheckBox Ch2VelOvAllCB;
    private System.Windows.Forms.CheckBox Ch4VelOvAllCB;
    private System.Windows.Forms.CheckBox Ch3VelOvAllCB;
    private System.Windows.Forms.CheckBox Ch8VelOvAllCB;
    private System.Windows.Forms.CheckBox Ch7VelOvAllCB;
    private System.Windows.Forms.CheckBox Ch6VelOvAllCB;
    private System.Windows.Forms.CheckBox Ch5VelOvAllCB;
    private System.Windows.Forms.CheckBox Ch8VelSpecCB;
    private System.Windows.Forms.CheckBox Ch7VelSpecCB;
    private System.Windows.Forms.CheckBox Ch6VelSpecCB;
    private System.Windows.Forms.CheckBox Ch5VelSpecCB;
    private System.Windows.Forms.CheckBox Ch4VelSpecCB;
    private System.Windows.Forms.CheckBox Ch3VelSpecCB;
    private System.Windows.Forms.CheckBox Ch2VelSpecCB;
    private System.Windows.Forms.CheckBox Ch1VelSpecCB;
    private System.Windows.Forms.Label label34;
    private System.Windows.Forms.CheckBox Ch8AccelSpecCB;
    private System.Windows.Forms.CheckBox Ch7AccelSpecCB;
    private System.Windows.Forms.CheckBox Ch6AccelSpecCB;
    private System.Windows.Forms.CheckBox Ch5AccelSpecCB;
    private System.Windows.Forms.CheckBox Ch4AccelSpecCB;
    private System.Windows.Forms.CheckBox Ch3AccelSpecCB;
    private System.Windows.Forms.CheckBox Ch2AccelSpecCB;
    private System.Windows.Forms.CheckBox Ch1AccelSpecCB;
    private System.Windows.Forms.Label label35;
    private System.Windows.Forms.CheckBox Ch8AccelOvAllCB;
    private System.Windows.Forms.CheckBox Ch7AccelOvAllCB;
    private System.Windows.Forms.CheckBox Ch6AccelOvAllCB;
    private System.Windows.Forms.CheckBox Ch5AccelOvAllCB;
    private System.Windows.Forms.CheckBox Ch4AccelOvAllCB;
    private System.Windows.Forms.CheckBox Ch3AccelOvAllCB;
    private System.Windows.Forms.CheckBox Ch2AccelOvAllCB;
    private System.Windows.Forms.CheckBox Ch1AccelOvAllCB;
    private System.Windows.Forms.Label label36;
    private System.Windows.Forms.CheckBox Ch8AccWFCB;
    private System.Windows.Forms.CheckBox Ch7AccWFCB;
    private System.Windows.Forms.CheckBox Ch6AccWFCB;
    private System.Windows.Forms.CheckBox Ch5AccWFCB;
    private System.Windows.Forms.CheckBox Ch4AccWFCB;
    private System.Windows.Forms.CheckBox Ch3AccWFCB;
    private System.Windows.Forms.CheckBox Ch2AccWFCB;
    private System.Windows.Forms.CheckBox Ch1AccWFCB;
    private System.Windows.Forms.Label label37;
    private System.Windows.Forms.Label label38;
    private System.Windows.Forms.ComboBox Ch8TrigTypeCB;
    private System.Windows.Forms.ComboBox Ch7TrigTypeCB;
    private System.Windows.Forms.ComboBox Ch6TrigTypeCB;
    private System.Windows.Forms.ComboBox Ch5TrigTypeCB;
    private System.Windows.Forms.ComboBox Ch4TrigTypeCB;
    private System.Windows.Forms.ComboBox Ch3TrigTypeCB;
    private System.Windows.Forms.ComboBox Ch2TrigTypeCB;
    private System.Windows.Forms.ComboBox Ch1TrigTypeCB;
    private System.Windows.Forms.Label label39;
    private System.Windows.Forms.Label label40;
    private System.Windows.Forms.ComboBox Ch8TrigStateValCB;
    private System.Windows.Forms.ComboBox Ch7TrigStateValCB;
    private System.Windows.Forms.ComboBox Ch6TrigStateValCB;
    private System.Windows.Forms.ComboBox Ch5TrigStateValCB;
    private System.Windows.Forms.ComboBox Ch4TrigStateValCB;
    private System.Windows.Forms.ComboBox Ch3TrigStateValCB;
    private System.Windows.Forms.ComboBox Ch2TrigStateValCB;
    private System.Windows.Forms.ComboBox Ch1TrigStateValCB;
    private System.Windows.Forms.TextBox Ch8RPMFloorTB;
    private System.Windows.Forms.TextBox Ch7RPMFloorTB;
    private System.Windows.Forms.TextBox Ch6RPMFloorTB;
    private System.Windows.Forms.TextBox Ch5RPMFloorTB;
    private System.Windows.Forms.TextBox Ch4RPMFloorTB;
    private System.Windows.Forms.TextBox Ch3RPMFloorTB;
    private System.Windows.Forms.TextBox Ch2RPMFloorTB;
    private System.Windows.Forms.TextBox Ch1RPMFloorTB;
    private System.Windows.Forms.Label label41;
    private System.Windows.Forms.Label label42;
    private System.Windows.Forms.Label label43;
    private System.Windows.Forms.Label label44;
    private System.Windows.Forms.Label label45;
    private System.Windows.Forms.Label label46;
    private System.Windows.Forms.Label label47;
    private System.Windows.Forms.Label label48;
    private System.Windows.Forms.Label label49;
    private System.Windows.Forms.Label label50;
    private System.Windows.Forms.Label label51;
    private System.Windows.Forms.Label label52;
    private System.Windows.Forms.Label label53;
    private System.Windows.Forms.Label label54;
    private System.Windows.Forms.Label label55;
    private System.Windows.Forms.Label label56;
    private System.Windows.Forms.Label label57;
    private System.Windows.Forms.TextBox Ch8RPMCeilingTB;
    private System.Windows.Forms.TextBox Ch7RPMCeilingTB;
    private System.Windows.Forms.TextBox Ch6RPMCeilingTB;
    private System.Windows.Forms.TextBox Ch5RPMCeilingTB;
    private System.Windows.Forms.TextBox Ch4RPMCeilingTB;
    private System.Windows.Forms.TextBox Ch3RPMCeilingTB;
    private System.Windows.Forms.TextBox Ch2RPMCeilingTB;
    private System.Windows.Forms.TextBox Ch1RPMCeilingTB;
    private System.Windows.Forms.Label label58;
    private System.Windows.Forms.TextBox Ch8SwtchOnThreshTB;
    private System.Windows.Forms.TextBox Ch7SwtchOnThreshTB;
    private System.Windows.Forms.TextBox Ch6SwtchOnThreshTB;
    private System.Windows.Forms.TextBox Ch5SwtchOnThreshTB;
    private System.Windows.Forms.TextBox Ch4SwtchOnThreshTB;
    private System.Windows.Forms.TextBox Ch3SwtchOnThreshTB;
    private System.Windows.Forms.TextBox Ch2SwtchOnThreshTB;
    private System.Windows.Forms.TextBox Ch1SwtchOnThreshTB;
    private System.Windows.Forms.Label label67;
    private System.Windows.Forms.Label label68;
    private System.Windows.Forms.Label label59;
    private System.Windows.Forms.TextBox Ch8SwtchOffThreshTB;
    private System.Windows.Forms.TextBox Ch7SwtchOffThreshTB;
    private System.Windows.Forms.TextBox Ch6SwtchOffThreshTB;
    private System.Windows.Forms.TextBox Ch5SwtchOffThreshTB;
    private System.Windows.Forms.TextBox Ch4SwtchOffThreshTB;
    private System.Windows.Forms.TextBox Ch3SwtchOffThreshTB;
    private System.Windows.Forms.TextBox Ch2SwtchOffThreshTB;
    private System.Windows.Forms.TextBox Ch1SwtchOffThreshTB;
    private System.Windows.Forms.Label label60;
    private System.Windows.Forms.Label label61;
    private System.Windows.Forms.Label label62;
    private System.Windows.Forms.Label label63;
    private System.Windows.Forms.Label label64;
    private System.Windows.Forms.Label label65;
    private System.Windows.Forms.Label label66;
    private System.Windows.Forms.Label label69;
    private System.Windows.Forms.Label label70;
    private System.Windows.Forms.Label label71;
    private System.Windows.Forms.Label label72;
    private System.Windows.Forms.Label label73;
    private System.Windows.Forms.Label label74;
    private System.Windows.Forms.Label label75;
    private System.Windows.Forms.Label label76;
    private System.Windows.Forms.Label label77;
    private System.Windows.Forms.Label label78;
    private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    private System.Windows.Forms.TextBox ForcedSecTB;
    private System.Windows.Forms.TextBox ForcedMinTB;
    private System.Windows.Forms.Label label79;
    private System.Windows.Forms.TextBox ForcedHrTB;
    private System.Windows.Forms.Label label81;
    private System.Windows.Forms.Label label82;
    private System.Windows.Forms.Label label80;
    private System.Windows.Forms.ComboBox Ch8LPFiltCB;
    private System.Windows.Forms.ComboBox Ch7LPFiltCB;
    private System.Windows.Forms.ComboBox Ch6LPFiltCB;
    private System.Windows.Forms.ComboBox Ch5LPFiltCB;
    private System.Windows.Forms.ComboBox Ch4LPFiltCB;
    private System.Windows.Forms.ComboBox Ch3LPFiltCB;
    private System.Windows.Forms.ComboBox Ch2LPFiltCB;
    private System.Windows.Forms.ComboBox Ch1LPFiltCB;
    private System.Windows.Forms.Label label83;
    private System.Windows.Forms.CheckBox Ch8DispSpecCB;
    private System.Windows.Forms.CheckBox Ch7DispSpecCB;
    private System.Windows.Forms.CheckBox Ch6DispSpecCB;
    private System.Windows.Forms.CheckBox Ch5DispSpecCB;
    private System.Windows.Forms.CheckBox Ch4DispSpecCB;
    private System.Windows.Forms.CheckBox Ch3DispSpecCB;
    private System.Windows.Forms.CheckBox Ch2DispSpecCB;
    private System.Windows.Forms.CheckBox Ch1DispSpecCB;
    private System.Windows.Forms.Label label84;
    private System.Windows.Forms.CheckBox Ch8DispOvAllCB;
    private System.Windows.Forms.CheckBox Ch7DispOvAllCB;
    private System.Windows.Forms.CheckBox Ch6DispOvAllCB;
    private System.Windows.Forms.CheckBox Ch5DispOvAllCB;
    private System.Windows.Forms.CheckBox Ch4DispOvAllCB;
    private System.Windows.Forms.CheckBox Ch3DispOvAllCB;
    private System.Windows.Forms.CheckBox Ch2DispOvAllCB;
    private System.Windows.Forms.CheckBox Ch1DispOvAllCB;
    private System.Windows.Forms.Label label85;
    */
    private System.Windows.Forms.Button OpenPortsBTN;
    private System.Windows.Forms.Button GetCommPortsBTN;
    private System.Windows.Forms.ComboBox CommPortsCB;
    private System.Windows.Forms.Label CommPortsLBL;
    private System.Windows.Forms.Label X8CommPortLBL;
    private System.Windows.Forms.Button ClosePortBtn;
        
    //private System.Windows.Forms.Button GetNumSavedBTN;
    private System.Windows.Forms.Timer EventTimer;
        private System.Windows.Forms.Button GetNumSavedMeasBTN;
        private System.Windows.Forms.Button DisplaySavedMeasBTN;
        private System.Windows.Forms.CheckedListBox measurement_clb;
        private System.Windows.Forms.Button SelectAllBTN;
        private System.Windows.Forms.Button SelectBetweenChecksBTN;
        private System.Windows.Forms.Button ClearAllBTN;
        private System.Windows.Forms.Button SaveToPCBTN;
        private System.Windows.Forms.Button GetX8TypeBTN;
        private System.Windows.Forms.Button GetDeviceIsPausedBTN;
        private System.Windows.Forms.DataVisualization.Charting.Chart SpectrumChart;
        private System.Windows.Forms.ListBox Debug_ListBox;
        private System.Windows.Forms.DataVisualization.Charting.Chart WaveformChart;
        private System.Windows.Forms.CheckBox InclSpectrumChkBox;
        private System.Windows.Forms.CheckBox InclWaveformChkBox;
        private System.Windows.Forms.TextBox PrimaryOverall_TB;
        private System.Windows.Forms.Label PrimaryOverall_LBL;
        private System.Windows.Forms.Label SecondaryOverall_LBL;
        private System.Windows.Forms.TextBox SecondaryOverall_TB;
        private System.Windows.Forms.CheckBox OverallsTimeoutChkBox;
        private System.Windows.Forms.Button JustLoadBTN;
        private System.Windows.Forms.Button SaveMeasurementsBTN;
        /*
private System.Windows.Forms.Label BottomLBL;
private System.Windows.Forms.Label label89;
private System.Windows.Forms.Label AlarmsLBL;
private System.Windows.Forms.Label VelRMSOvAllAlarmA;
private System.Windows.Forms.Label AccRMSOvAllAlarmA;
private System.Windows.Forms.TextBox VelRMSOvAll1AlarmTB;
private System.Windows.Forms.TextBox VelRMSOvAll2AlarmTB;
private System.Windows.Forms.TextBox VelRMSOvAll3AlarmTB;
private System.Windows.Forms.TextBox VelRMSOvAll4AlarmTB;
private System.Windows.Forms.TextBox VelRMSOvAll5AlarmTB;
private System.Windows.Forms.TextBox VelRMSOvAll6AlarmTB;
private System.Windows.Forms.TextBox VelRMSOvAll7AlarmTB;
private System.Windows.Forms.TextBox VelRMSOvAll8AlarmTB;
private System.Windows.Forms.TextBox AccRMSOvAll8AlarmTB;
private System.Windows.Forms.TextBox AccRMSOvAll7AlarmTB;
private System.Windows.Forms.TextBox AccRMSOvAll6AlarmTB;
private System.Windows.Forms.TextBox AccRMSOvAll5AlarmTB;
private System.Windows.Forms.TextBox AccRMSOvAll4AlarmTB;
private System.Windows.Forms.TextBox AccRMSOvAll3AlarmTB;
private System.Windows.Forms.TextBox AccRMSOvAll2AlarmTB;
private System.Windows.Forms.TextBox AccRMSOvAll1AlarmTB;
private System.Windows.Forms.TextBox VelRMSOvAll8WarningTB;
private System.Windows.Forms.TextBox VelRMSOvAll7WarningTB;
private System.Windows.Forms.TextBox VelRMSOvAll6WarningTB;
private System.Windows.Forms.TextBox VelRMSOvAll5WarningTB;
private System.Windows.Forms.TextBox VelRMSOvAll4WarningTB;
private System.Windows.Forms.TextBox VelRMSOvAll3WarningTB;
private System.Windows.Forms.TextBox VelRMSOvAll2WarningTB;
private System.Windows.Forms.TextBox VelRMSOvAll1WarningTB;
private System.Windows.Forms.Label VelRMSOvAllAlarmW;
private System.Windows.Forms.Label VelAlarmUnitLBL;
private System.Windows.Forms.Label VelWarningUnitLBL;
private System.Windows.Forms.Label AccAlarmUnitLBL;
private System.Windows.Forms.Label AccWarningUnitLBL;
private System.Windows.Forms.TextBox AccRMSOvAll8WarningTB;
private System.Windows.Forms.TextBox AccRMSOvAll7WarningTB;
private System.Windows.Forms.TextBox AccRMSOvAll6WarningTB;
private System.Windows.Forms.TextBox AccRMSOvAll5WarningTB;
private System.Windows.Forms.TextBox AccRMSOvAll4WarningTB;
private System.Windows.Forms.TextBox AccRMSOvAll3WarningTB;
private System.Windows.Forms.TextBox AccRMSOvAll2WarningTB;
private System.Windows.Forms.TextBox AccRMSOvAll1WarningTB;
private System.Windows.Forms.Label AccRMSOvAllAlarmW;
private System.Windows.Forms.CheckBox PCTimeUpdateCB;
private System.Windows.Forms.Label FirmwareControlLBL;
private System.Windows.Forms.Label CurrVersionLBL;
private System.Windows.Forms.TextBox CurrFWVerTB;
private System.Windows.Forms.Button LoadFWBTN;
private System.Windows.Forms.CheckBox strictAdherenceToIntervalCB;
private System.Windows.Forms.ProgressBar FWProgressBar;
private System.Windows.Forms.Label label90;
private System.Windows.Forms.Label DispWarnUnitLBL;
private System.Windows.Forms.TextBox DispPkPkOvAll8WarnTB;
private System.Windows.Forms.TextBox DispPkPkOvAll7WarnTB;
private System.Windows.Forms.TextBox DispPkPkOvAll6WarnTB;
private System.Windows.Forms.TextBox DispPkPkOvAll5WarnTB;
private System.Windows.Forms.TextBox DispPkPkOvAll4WarnTB;
private System.Windows.Forms.TextBox DispPkPkOvAll3WarnTB;
private System.Windows.Forms.TextBox DispPkPkOvAll2WarnTB;
private System.Windows.Forms.TextBox DispPkPkOvAll1WarnTB;
private System.Windows.Forms.Label DispPkPkOvAllWarning;
private System.Windows.Forms.Label DispAlarmUnitLBL;
private System.Windows.Forms.TextBox DispPkPkOvAll8AlarmTB;
private System.Windows.Forms.TextBox DispPkPkOvAll7AlarmTB;
private System.Windows.Forms.TextBox DispPkPkOvAll6AlarmTB;
private System.Windows.Forms.TextBox DispPkPkOvAll5AlarmTB;
private System.Windows.Forms.TextBox DispPkPkOvAll4AlarmTB;
private System.Windows.Forms.TextBox DispPkPkOvAll3AlarmTB;
private System.Windows.Forms.TextBox DispPkPkOvAll2AlarmTB;
private System.Windows.Forms.TextBox DispPkPkOvAll1AlarmTB;
private System.Windows.Forms.Label DispPkPkOvAllAlarm;
private System.Windows.Forms.Label DispLowLowUnitLBL;
private System.Windows.Forms.TextBox DispPkPkOvAll8LowLowTB;
private System.Windows.Forms.TextBox DispPkPkOvAll7LowLowTB;
private System.Windows.Forms.TextBox DispPkPkOvAll6LowLowTB;
private System.Windows.Forms.TextBox DispPkPkOvAll5LowLowTB;
private System.Windows.Forms.TextBox DispPkPkOvAll4LowLowTB;
private System.Windows.Forms.TextBox DispPkPkOvAll3LowLowTB;
private System.Windows.Forms.TextBox DispPkPkOvAll2LowLowTB;
private System.Windows.Forms.TextBox DispPkPkOvAll1LowLowTB;
private System.Windows.Forms.Label DispPkPkOvAllLowLow;
private System.Windows.Forms.Label AccLowLowUnitLBL;
private System.Windows.Forms.TextBox AccRMSOvAll8LowLowTB;
private System.Windows.Forms.TextBox AccRMSOvAll7LowLowTB;
private System.Windows.Forms.TextBox AccRMSOvAll6LowLowTB;
private System.Windows.Forms.TextBox AccRMSOvAll5LowLowTB;
private System.Windows.Forms.TextBox AccRMSOvAll4LowLowTB;
private System.Windows.Forms.TextBox AccRMSOvAll3LowLowTB;
private System.Windows.Forms.TextBox AccRMSOvAll2LowLowTB;
private System.Windows.Forms.TextBox AccRMSOvAll1LowLowTB;
private System.Windows.Forms.Label AccRMSOvAllLowLowLBL;
private System.Windows.Forms.Label VelLowLowUnitLBL;
private System.Windows.Forms.TextBox VelRMSOvAll8LowLowTB;
private System.Windows.Forms.TextBox VelRMSOvAll7LowLowTB;
private System.Windows.Forms.TextBox VelRMSOvAll6LowLowTB;
private System.Windows.Forms.TextBox VelRMSOvAll5LowLowTB;
private System.Windows.Forms.TextBox VelRMSOvAll4LowLowTB;
private System.Windows.Forms.TextBox VelRMSOvAll3LowLowTB;
private System.Windows.Forms.TextBox VelRMSOvAll2LowLowTB;
private System.Windows.Forms.TextBox VelRMSOvAll1LowLowTB;
private System.Windows.Forms.Label VelRMSOvAllLowLowLBL;
private System.Windows.Forms.TextBox COLL8TB2;
private System.Windows.Forms.TextBox COLL7TB2;
private System.Windows.Forms.TextBox COLL6TB2;
private System.Windows.Forms.TextBox COLL5TB2;
private System.Windows.Forms.TextBox COLL4TB2;
private System.Windows.Forms.TextBox COLL3TB2;
private System.Windows.Forms.TextBox COLL2TB2;
private System.Windows.Forms.TextBox COLL1TB2;
private System.Windows.Forms.Label Collections2LBL;
private System.Windows.Forms.Label label19;
private System.Windows.Forms.Label SensValLBL;
private System.Windows.Forms.TextBox Ch1SensTB;
private System.Windows.Forms.TextBox Ch2SensTB;
private System.Windows.Forms.TextBox Ch3SensTB;
private System.Windows.Forms.TextBox Ch4SensTB;
private System.Windows.Forms.TextBox Ch5SensTB;
private System.Windows.Forms.TextBox Ch6SensTB;
private System.Windows.Forms.TextBox Ch7SensTB;
private System.Windows.Forms.TextBox Ch8SensTB;
private System.Windows.Forms.Label SensVal1LBL;
private System.Windows.Forms.Label SensVal2LBL;
private System.Windows.Forms.Label SensVal3LBL;
private System.Windows.Forms.Label SensVal4LBL;
private System.Windows.Forms.Label SensVal5LBL;
private System.Windows.Forms.Label SensVal6LBL;
private System.Windows.Forms.Label SensVal7LBL;
private System.Windows.Forms.Label SensVal8LBL;
*/
        //private System.ComponentModel.BackgroundWorker FWProgressbackgroundWorker;

    }
      
}

