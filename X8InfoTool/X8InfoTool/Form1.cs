﻿//#define SLOW_VERSION


using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Text;
using System.Windows.Forms;
using XModemProtocol;
using System.Windows.Forms.DataVisualization.Charting;


namespace X8InfoTool
{

    public partial class X8InfoToolForm : Form
    {
        const UInt16 MAX_WAVEFORM_LEN = 8192;
        Int32 timerWait;
        bool timerFinished;
        Int32[] SerialBufPointers = new Int32[2];
        Byte[] SerialBuffer = new Byte[16384];
        Byte[] SpectrumBufferBytes = new Byte[3200*4*2];
        float[] spectrumBuffer = new float[3200*2];
        Byte[] WaveformBufferBytes = new Byte[MAX_WAVEFORM_LEN * 4* 2];
        float[] WaveformBuffer = new float[MAX_WAVEFORM_LEN * 2];
        bool deviceIsX8II = true;
        //Int32 WIFI_SSID_LENGTH = 32 + 1;
        Byte deviceIsPaused = 0;
        const Byte NUM_OVERALLS = 19;
        float[] overalls_pad = new float[NUM_OVERALLS];
        float[] overalls = new float[NUM_OVERALLS];
        List<UInt16> blockLocList = new List<UInt16>();
        List<bool> destIncludesMD = new List<bool>();
        List<bool> destIncludesEMonitor = new List<bool>();
        UInt32 appVer = 0;
        Byte dType = 0;
        UInt32 measDataLength = 0;
        UInt32 maxLOR = 3200;
        UInt32 nextSampleTime = 0;
        UInt32 timeStampS = 0;
        float inputRMS = 0.0f;
        float maxFrequency = 0.0f;
        Byte channelTested = 0;
        Byte spectralBandLimitError = 0;
        UInt32 spectrumLength = 0;
        UInt32 sampling_frequency = 0;
        float maxBinValue = 0.0f;
        UInt32 maxBinIndex = 0;
        UInt32 waveformLength = 0;
        Byte destinations;
        const UInt16 LARGE_BLOCK_PAYLOAD_SIZE = 10000;
        bool receivingSpectrum = false;
        MDTransducerTypes transducerType = MDTransducerTypes.Accelerometer;

        //CheckedListBox measurement_clb = new CheckedListBox();

        enum X8TYPE
        {
            X8TYPE_UNKNOWN  = 0,
            X8TYPE_X8       = 1,
            X8TYPE_X8II     = 2
        };

        const Single MILS_TO_MICROMETERS = 25.4f;

        enum ASCII_TABLE
        {
            ASCII_NUL = 0,
            ASCII_SOH = 1,
            ASCII_STX = 2,
            ASCII_ETX = 3,
            ASCII_EOT = 4,
            ASCII_ENQ = 5,
            ASCII_ACK = 6,
            ASCII_BEL = 7,
            ASCII_BS = 8,
            ASCII_HT = 9,
            ASCII_LF = 10,
            ASCII_VT = 11,
            ASCII_FF = 12,
            ASCII_CR = 13,
            ASCII_SO = 14,
            ASCII_SI = 15,
            ASCII_DLE = 16,
            ASCII_DC1 = 17,
            ASCII_DC2 = 18,
            ASCII_DC3 = 19,
            ASCII_DC4 = 20,
            ASCII_NAK = 21,
            ASCII_SYN = 22,
            ASCII_ETB = 23,
            ASCII_CAN = 24,
            ASCII_EM = 25,
            ASCII_SUB = 26,
            ASCII_ESC = 27,
            ASCII_FS = 28,
            ASCII_GS = 29,
            ASCII_RS = 30,
            ASCII_US = 31,
            ASCII_SP = 32,
            ASCII_BANG = 33,
            ASCII_DQUOTE = 34,
            ASCII_HASH = 35,
            ASCII_DOLLAR = 36,
            ASCII_PERCENT = 37,
            ASCII_AND = 38,
            ASCII_SQUOTE = 39,
            ASCII_LPAREN = 40,
            ASCII_RPAREN = 41,
            ASCII_STAR = 42,
            ASCII_PLUS = 43,
            ASCII_COMMA = 44,
            ASCII_MINUS = 45,
            ASCII_DOT = 46,
            ASCII_SLASH = 47,
            ASCII_0 = 48,
            ASCII_1 = 49,
            ASCII_2 = 50,
            ASCII_3 = 51,
            ASCII_4 = 52,
            ASCII_5 = 53,
            ASCII_6 = 54,
            ASCII_7 = 55,
            ASCII_8 = 56,
            ASCII_9 = 57,
            ASCII_COLON = 58,
            ASCII_SEMICOLON = 59,
            ASCII_LARROW = 60,
            ASCII_EQUALS = 61,
            ASCII_RARROW = 62,
            ASCII_QUESTION = 63,
            ASCII_AT = 64,
            ASCII_A = 65,
            ASCII_B = 66,
            ASCII_C = 67,
            ASCII_D = 68,
            ASCII_E = 69,
            ASCII_F = 70,
            ASCII_G = 71,
            ASCII_H = 72,
            ASCII_I = 73,
            ASCII_J = 74,
            ASCII_K = 75,
            ASCII_L = 76,
            ASCII_M = 77,
            ASCII_N = 78,
            ASCII_O = 79,
            ASCII_P = 80,
            ASCII_Q = 81,
            ASCII_R = 82,
            ASCII_S = 83,
            ASCII_T = 84,
            ASCII_U = 85,
            ASCII_V = 86,
            ASCII_W = 87,
            ASCII_X = 88,
            ASCII_Y = 89,
            ASCII_Z = 90,
            ASCII_LBRACKET = 91,
            ASCII_BACKSLASH = 92,
            ASCII_RBRACKET = 93,
            ASCII_CARET = 94,
            ASCII__ = 95,
            ASCII_BACKTICK = 96,
            ASCII_a = 97,
            ASCII_b = 98,
            ASCII_c = 99,
            ASCII_d = 100,
            ASCII_e = 101,
            ASCII_f = 102,
            ASCII_g = 103,
            ASCII_h = 104,
            ASCII_i = 105,
            ASCII_j = 106,
            ASCII_k = 107,
            ASCII_l = 108,
            ASCII_m = 109,
            ASCII_n = 110,
            ASCII_o = 111,
            ASCII_p = 112,
            ASCII_q = 113,
            ASCII_r = 114,
            ASCII_s = 115,
            ASCII_t = 116,
            ASCII_u = 117,
            ASCII_v = 118,
            ASCII_w = 119,
            ASCII_x = 120,
            ASCII_y = 121,
            ASCII_z = 122,
            ASCII_LBRACE = 123,
            ASCII_PIPE = 124,
            ASCII_RBRACE = 125,
            ASCII_TILDE = 126,
            ASCII_DEL = 127,
        };


        BindingList<string> commPorts = new BindingList<string>();
        SerialPort commPort;
        string chosenPortName;
        string commPortReadData;


        public X8InfoToolForm()
        {
            InitializeComponent();
            //setDefaults();
            SpectrumChart.Series.Clear();
            WaveformChart.Series.Clear();
        }

        ~X8InfoToolForm()
        {
            if (commPort != null && commPort.IsOpen)
            {
                commPort.Close();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // Show all the incoming data in the port's buffer
            commPortReadData += commPort.ReadExisting();
            //MessageBox.Show(commPort.ReadExisting());
        }

        private void OpenPortsBTN_Click(object sender, EventArgs e)
        {
            //object ZarkCommPort = new IO.Ports.SerialPort;
            //ZarkCommPort.PortName = My.Settings.ZarkComm
            string commPortName = String.Empty;
            try
            {
                commPortName = commPorts[CommPortsCB.SelectedIndex];

                // This name includes the comm port description.  Split it to get the comm port name
                string[] splits = commPortName.Split(' ');
                commPortName = splits[0];

            }
            catch (Exception ex)
            {
                string msgString = ex.Message + System.Environment.NewLine + "Have you selected a comm port?";
                MessageBox.Show(msgString);
                return;
            }

            chosenPortName = commPortName;

            int baudRate = 115200;
            commPort = new SerialPort(commPortName, baudRate, Parity.None, 8, StopBits.One);
            commPort.DtrEnable = true;
            commPort.RtsEnable = true;
            commPort.Handshake = Handshake.None;
            commPort.ReadBufferSize = 100000;

            // Attach a method to be called when there
            // is data waiting in the port's buffer
            //commPort.DataReceived += new
            //SerialDataReceivedEventHandler(port_DataReceived);

            MessageBox.Show("Opening Port.  Please wait until the x8 comm port label turns green and a confirmation message pops up that the X8 port is open before sending or retrieving from the X8.");

            // Open the port
            while (!commPort.IsOpen)
            {
                if (!openCommPort())
                    return;
            }

            Application.UseWaitCursor = false;
        }
        
        private bool openCommPort()
        {   
            Application.UseWaitCursor = true;
            try
            {
                commPort.Open();
            }
            catch (Exception ioe)
            {
                string exceptionMsg = ioe.Message + System.Environment.NewLine + "Make sure Tera Term is disconnected.";
                MessageBox.Show(exceptionMsg);
                Application.UseWaitCursor = false;
                return false;
            }

            if (commPort.IsOpen)
            {
                char[] sendString = new char[10];
                Byte strLen = 10;
                if (deviceIsX8II)
                {
                    // pause the application
                    //commPort.WriteLine("shinkawa");
                    sendString[0] = 's';
                    sendString[1] = 'h';
                    sendString[2] = 'i';
                    sendString[3] = 'n';
                    sendString[4] = 'k';
                    sendString[5] = 'a';
                    sendString[6] = 'w';
                    sendString[7] = 'a';
                    sendString[8] = (char)13; // cr
                    strLen = 9;
                }
                else
                {
                    // pause the application
                    //commPort.WriteLine("_awaknihs");
                    sendString[0] = '_';
                    sendString[1] = 'a';
                    sendString[2] = 'w';
                    sendString[3] = 'a';
                    sendString[4] = 'k';
                    sendString[5] = 'n';
                    sendString[6] = 'i';
                    sendString[7] = 'h';
                    sendString[8] = 's';
                    sendString[9] = (char)13; // cr
                    strLen = 10;
                }

                
                SerialBufPointers[0] = 0;
                SerialBufPointers[1] = 0;
                try
                {
                    commPort.Write(sendString, 0, strLen);
                    timerWait = 150;
                    timerFinished = false;
                    EventTimer.Start();
                    while (SerialBufPointers[0] < 1 && timerFinished == false)
                        Application.DoEvents();
                    EventTimer.Stop();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    //failedCommunication = True
                    return true;
                }

                EventTimer.Stop();

                //if (timerFinished)
                //{
                //    MessageBox.Show("Password timed-out!");
                //    //failedCommunication = True
                //    return true;
                //}

                timeDelayMS(50);

                placeDeviceInTestMode();

                while (deviceIsPaused == 0)
                {
                    GetDeviceIsPaused();
                }

                // do it twice
                deviceIsPaused = 0;
                while (deviceIsPaused == 0)
                {
                    GetDeviceIsPaused();
                }

                deviceIsX8II = false;
                X8TYPE x8Type = checkX8Type();
                if (x8Type == X8TYPE.X8TYPE_X8II)
                    deviceIsX8II = true;


                //while (!isInHardwareTestMode())
                //{
                //    Application.DoEvents();
                //}
                // Enable the ClosePort button
                Application.UseWaitCursor = false;
                //MessageBox.Show("Comm Port " + commPortName + " is now open");
                X8CommPortLBL.Enabled = true;
                X8CommPortLBL.ForeColor = Color.Green;

                // Disable the OpenPort button
                OpenPortsBTN.Enabled = false;

                // Enable the read buffer button
                //GetNumSavedBTN.Enabled = true;
                ClosePortBtn.Enabled = true;
                MessageBox.Show("Port is open.  You may send or retrieve from the X8.");
                //SendBTN.Enabled = true;
                //RetrieveBTN.Enabled = true;
                ClosePortBtn.Enabled = true;
            }
            return true;
        }


        private void GetCommPortsBTN_Click(object sender, EventArgs e)
        {
            // Get a list of serial port names.

            using (var searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PnPEntity WHERE Caption like '%(COM%'"))
            {
                var portnames = SerialPort.GetPortNames();
                var ports = searcher.Get().Cast<ManagementBaseObject>().ToList().Select(p => p["Caption"].ToString());

                var portList = portnames.Select(n => n + " - " + ports.FirstOrDefault(s => s.Contains(n))).ToList();

                // Display each port name to the console.
                commPorts.Clear();
                foreach (string portName in portList)
                {
                    if (!stringIsInBindingList(portName, commPorts))
                        commPorts.Add(portName);
                }
                CommPortsCB.DataSource = commPorts;
                try
                {
                    CommPortsCB.SelectedIndex = 0;
                }
                catch (Exception ioe)
                {
                    string exceptionMsg = ioe.Message + System.Environment.NewLine + "Make sure the X8 is connected by USB cable.";
                    MessageBox.Show(exceptionMsg);
                    Application.UseWaitCursor = false;
                }

            }
        }

        bool stringIsInBindingList(string srchString_, BindingList<string> list_)
        {
            foreach (string tmpStr in list_)
            {
                if (tmpStr == srchString_)
                    return true;
            }
            return false;
        }

        bool stringIsInArray(string srchString_, string[] array_)
        {
            foreach (string tmpStr in array_)
            {
                if (tmpStr == srchString_)
                    return true;
            }
            return false;
        }

        private void ClosePortBtn_Click(object sender, EventArgs e)
        {
            if (commPort == null)
                return;
            if (commPort.IsOpen)
            {
                // put the X8 back in normal run mode
                placeDeviceInNormalRunMode();
                sendRun();
                try
                {
                    commPort.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return;
                }
            }
            X8CommPortLBL.Enabled = false;
            X8CommPortLBL.ForeColor = Color.Gray;

            // Enable the OpenPort button
            OpenPortsBTN.Enabled = true;

            // Disable the read buffer button
            //GetNumSavedBTN.Enabled = false;

            // Disable the ClosePort button
            ClosePortBtn.Enabled = false;

            //SendBTN.Enabled = false;
            //RetrieveBTN.Enabled = false;

        }

        // issue pause
        private void issuePauseApp()
        {
            //Byte cmdArr[4] = {1,  };
            //commPort.Write();
        }

        private void placeDeviceInTestMode()
        {
            // CMDPauseDevice = 235
            //char[] sendString = new char[5];

            //sendString[0] = 'S';
            //sendString[1] = 'T';
            //sendString[2] = 'H';
            //sendString[3] = 'W';
            //sendString[4] = (char)13;

            Byte[] sendString = new Byte[3];

            sendString[0] = 1;
            sendString[1] = 235;
            sendString[2] = 0xFF;

            SerialBufPointers[0] = 0;
            SerialBufPointers[1] = 0;

            //string returnString = "Device in HW Test Modes\r\n";
            try
            {
                
                timerWait = 25000;
                timerFinished = false;
                EventTimer.Start();
                commPort.Write(sendString, 0, 3);
                //while (SerialBufPointers[0] < returnString.Length && timerFinished == false)
                while (SerialBufPointers[0] < 1 && timerFinished == false)
                        Application.DoEvents();
                EventTimer.Stop();
                SerialBufPointers[0] = 0; // we don't care what gets sent back
                SerialBufPointers[1] = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //failedCommunication = True
                return;
            }

            //if (timerFinished)
            //{
            //    MessageBox.Show("Failed to set device in Hardware Test Mode!");
            //    return;
            //}

            timeDelayMS(500);
            //closePorts()
        }

        private void placeDeviceInNormalRunMode()
        {
            //char[] sendString = new char[4];
            Byte[] sendString = new Byte[3];

            //sendString[0] = 'R';
            //sendString[1] = 'U';
            //sendString[2] = 'N';
            //sendString[3] = (char)13;

            sendString[0] = 1;
            sendString[1] = 61;
            sendString[2] = 0xFF;

            SerialBufPointers[0] = 0;
            SerialBufPointers[1] = 0;

            try
            {
                commPort.Write(sendString, 0, 3);
                timerWait = 150;
                timerFinished = false;
                EventTimer.Start();
                while (SerialBufPointers[0] < 1 && timerFinished == false)
                    Application.DoEvents();
                EventTimer.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //failedCommunication = True
                return;
            }
            timeDelayMS(500);
            deviceIsPaused = 0;
            //closePorts()
        }

        private void sendRun()
        {
            char[] sendString = new char[4];

            sendString[0] = 'R';
            sendString[1] = 'U';
            sendString[2] = 'N';
            sendString[3] = (char)13;


            SerialBufPointers[0] = 0;
            SerialBufPointers[1] = 0;

            try
            {
                commPort.Write(sendString, 0, 4);
                timerWait = 150;
                timerFinished = false;
                EventTimer.Start();
                while (SerialBufPointers[0] < 3 && timerFinished == false)
                    Application.DoEvents();
                EventTimer.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //failedCommunication = True
                return;
            }
            timeDelayMS(500);
            //closePorts()
        }
        

        private void timeDelayMS(UInt16 ms_)
        {
            System.Threading.Thread.Sleep(ms_);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            EventTimer.Enabled = false;
            if (timerWait > 0)
                timerWait = timerWait - 1;
            else
                timerFinished = true;

            try
            {
                if (commPort != null && commPort.IsOpen)
                {
                    while (commPort.BytesToRead > 0)
                    {
                        SerialBuffer[SerialBufPointers[0]] = (Byte)commPort.ReadByte();
                        //if (receivingSpectrum && SerialBufPointers[0] < 50)
                        //    debugLog("SerialBuffer[" + SerialBufPointers[0].ToString() + "]:  " + SerialBuffer[SerialBufPointers[0]]);
                        SerialBufPointers[0] = SerialBufPointers[0] + 1;
                        //SerialBufPointers[0] &= 0xFFF;
                        SerialBufPointers[0] &= 0x3FFF;

                        //MessageBox.Show("SerialBufPointers[0]: " + SerialBufPointers.ToString());
                        Application.DoEvents();
                    }
                }
            }
            catch (Exception ex)
            {
                if (commPort.IsOpen)
                    MessageBox.Show(ex.Message);
            }
            EventTimer.Enabled = true;
        }

        enum MDTransducerTypes
        {
            Accelerometer = 0x00,
            ProximityProbe = 0x01,
            StrainProbe = 0x02,
            DCVoltageProbe = 0x03,
            TemperatureProbe = 0x04,
            GenericProbe = 0x05
        };

        enum SpectrumTypes
        {
            spec_displacement = 0x00,
            spec_velocity = 0x01,
            spec_acceleration = 0x02,
            spec_microstrain = 0x03,
            spec_generic = 0x04,
            spec_none = 0x05
        };

        enum AccelerometerSensitivity
        {
            ACC_SENS_10 = 0,
            ACC_SENS_50 = 1,
            ACC_SENS_100 = 2,
            ACC_SENS_500 = 3
        };
        enum StrainSensitivity
        {
            STRAIN_SENS_50 = 0
        };

        enum ProximitySensitivity
        {
            PROX_SENS_200 = 0
        };

        enum TemperatureSensitivity
        {
            TEMP_SENS_10 = 0,
            TEMP_SENS_20 = 1
        };

        private void loadChannelConfig(Byte ch_)
        {

            Byte[] sendString = new Byte[4];
            sendString[0] = 1;
            sendString[1] = 152;  // loadChannelConfig
            sendString[2] = ch_;
            sendString[3] = 0xFF;

            SerialBufPointers[0] = 0;
            SerialBufPointers[1] = 0;
            try
            {
                commPort.Write(sendString, 0, 4);
                timerWait = 150;
                timerFinished = false;
                EventTimer.Start();
                while (SerialBufPointers[0] < 3 && timerFinished == false)
                    Application.DoEvents();
                EventTimer.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //failedCommunication = True
                return;
            }
            timeDelayMS(50);
        }


        private void saveChannelConfig(Byte ch_)
        {

            Byte[] sendString = new Byte[4];
            sendString[0] = 1;
            sendString[1] = 153;  // saveChannelConfig
            sendString[2] = ch_;
            sendString[3] = 0xFF;

            SerialBufPointers[0] = 0;
            SerialBufPointers[1] = 0;
            try
            {
                commPort.Write(sendString, 0, 4);
                timerWait = 150;
                timerFinished = false;
                EventTimer.Start();
                while (SerialBufPointers[0] < 3 && timerFinished == false)
                    Application.DoEvents();
                EventTimer.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //failedCommunication = True
                return;
            }
            timeDelayMS(50);
        }

        enum HP_filters
        {
            HP_OFF = 0x00,
            HP10 = 0x01, //DEFAULT
            HP5 = 0x02,
            HP2 = 0x03,
            HP25 = 0x04,
            HP1K = 0x05
        };

        enum LP_filters
        {
            LP_OFF = 0x00,
            LP_50 = 0x01,
            LP_100 = 0x02,
            LP_200 = 0x03,
            LP_500 = 0x04,
            LP_750 = 0x05,
            LP_1K = 0x06,   //DEFAULT
            LP_5K = 0x07
        };

        enum RawSignalFormats
        {
            vib_ch_format_raw = 0,
            vib_ch_format_hf_only = 1,
            vib_ch_format_hp_filtered = 2,
            vib_ch_format_integrated = 3,
            vib_ch_format_hp_lp_filtered = 4,
            vib_ch_format_user_input_A = 5,
            vib_ch_format_user_input_B = 6,
            vib_ch_format_user_input_C = 7
        };

        enum MeasRates
        {
            SEC_30 = 0x00,  //0
            MIN_1 = 0x01,   //1
            MIN_2 = 0x02,   //2
            MIN_5 = 0x03,   //3
            MIN_10 = 0x04,  //4
            MIN_20 = 0x05,  //5
            MIN_30 = 0x06,  //6
            H_1 = 0x07, //7
            H_2 = 0x08, //8
            H_3 = 0x09, //9
            H_4 = 0x0A, //10
            H_6 = 0x0B, //11  //Default Alarm Rate
            H_12 = 0x0C,    //12	//Default Normal Rate
            D_1 = 0x0D, //13
            W_1 = 0x0E, //14	//TODO-Don't have timer implementation for this
            M_1 = 0x0F, //15	//TODO-Don't have timer implementation for this
        };

        enum Fmax_Values
        {
            Fmax_500Hz = 0,   // Not visible to user in MD 2.6
            Fmax_1000Hz = 1,   // Not visible to user in MD 2.6
            Fmax_1500Hz = 2,   // Visible to user in MD 2.6
            Fmax_3000Hz = 3,   // Visible to user in MD 2.6 ; Default Resolution
            Fmax_5000Hz = 4,   // Visible to user in MD 2.6
            Fmax_10000Hz = 5,   // Visible to user in MD 2.6
            Fmax_18300Hz = 6    // Visible to user in MD 2.6
        };

        enum SpectrumResolutionsNew
        {
            NEW_RES_200 = 0,  //200 Lines
            NEW_RES_400 = 1,  //400 Lines
            NEW_RES_800 = 2,  //800 Lines
            NEW_RES_1600 = 3,  //1600 Lines
            NEW_RES_3200 = 4,  //3200 Lines
            NEW_RES_6400 = 5,  //6400 Lines
            NEW_RES_12800 = 6   //12800 Lines
        };

        public static byte[] ConvertDoubleToByteArray(double d)
        {
            return BitConverter.GetBytes(d);
        }

        public static double ConvertByteArrayToDouble(byte[] b)
        {
            return BitConverter.ToDouble(b, 0);
        }

        enum SpeedTriggerPercent
        {
            SPEED_TRIGGER_PCT_10 = 0,
            SPEED_TRIGGER_PCT_20 = 1,
            SPEED_TRIGGER_PCT_30 = 2,
            SPEED_TRIGGER_PCT_40 = 3,
            SPEED_TRIGGER_PCT_50 = 4,
            SPEED_TRIGGER_PCT_60 = 5,
            SPEED_TRIGGER_PCT_70 = 6,
            SPEED_TRIGGER_PCT_80 = 7,
            SPEED_TRIGGER_PCT_90 = 8,
        };

        enum SpeedTrigger
        {
            TRIGGER_BY_STATE,
            TRIGGER_BY_SWITCH,
            TRIGGER_BY_SPEED_FLOOR,
            TRIGGER_BY_SPEED_CEILING,
            TRIGGER_BY_SPEED_RANGE
        };

        private bool isInHardwareTestMode()
        {
            // getHardwareTestFlag     = 193
            Byte[] sendString = new Byte[3];
            sendString[0] = 1;
            sendString[1] = 193;  // getHardwareTestFlag
            sendString[2] = 0xFF;

            // clear the serial buffer
            EventTimer.Start();
            timeDelayMS(10);
            EventTimer.Stop();

            SerialBufPointers[0] = 0;
            SerialBufPointers[1] = 0;

            try
            {
                commPort.Write(sendString, 0, 3);
                timerWait = 150;
                timerFinished = false;
                EventTimer.Start();
                while (SerialBufPointers[0] < 1 && timerFinished == false)
                    Application.DoEvents();
                EventTimer.Stop();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                //failedCommunication = True
                return false;
            }

            Byte tmp = SerialBuffer[0];
            bool hwTestActive = false;
            if (tmp == 1)
                hwTestActive = true;

            return (bool)hwTestActive;
        }

        enum FirmwareStatus
        {
            FIRMWARE_UNDETERMINED,
            FIRMWARE_OK,
            FIRMWARE_EXISTS = 50,
            FIRMWARE_OLD = 55,
        }

        private FirmwareStatus checkFirmwareStatus(UInt32 version_)
        {
            // checkIfFWVersionValid   = 200,
            Byte[] versionBytes = BitConverter.GetBytes(version_);
            Byte[] sendString = new Byte[7];
            sendString[0] = 1;
            sendString[1] = 200;  // checkIfFWVersionValid
            sendString[2] = versionBytes[0];
            sendString[3] = versionBytes[1];
            sendString[4] = versionBytes[2];
            sendString[5] = versionBytes[3];
            sendString[6] = 0xFF;

            // clear the serial buffer
            EventTimer.Start();
            timeDelayMS(10);
            EventTimer.Stop();

            SerialBufPointers[0] = 0;
            SerialBufPointers[1] = 0;

            try
            {
                commPort.Write(sendString, 0, 7);
                timerWait = 150;
                timerFinished = false;
                timeDelayMS(200);
                EventTimer.Start();
                while (SerialBufPointers[0] < 1 && timerFinished == false)
                    Application.DoEvents();
                EventTimer.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return FirmwareStatus.FIRMWARE_UNDETERMINED;
            }

            return (FirmwareStatus)SerialBuffer[0];

        }

        private bool chosenPortIsInactive()
        {
            BindingList<string> tmpcommPorts = new BindingList<string>();
            using (var searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PnPEntity WHERE Caption like '%(COM%'"))
            {
                string[] portnames = SerialPort.GetPortNames();
                if (!stringIsInArray(chosenPortName, portnames))
                    return true;
            }
            return false;
        }

        private X8TYPE checkX8Type()
        {
            // CMDGetX8Type			     = 226
            Byte[] sendString = new Byte[3];
            sendString[0] = 1;
            sendString[1] = 226;  // CMDGetX8Type
            sendString[2] = 0xFF;

            // clear the serial buffer
            EventTimer.Start();
            timeDelayMS(10);
            EventTimer.Stop();

            SerialBufPointers[0] = 0;
            SerialBufPointers[1] = 0;

            try
            {
                commPort.Write(sendString, 0, 3);
                timerWait = 150;
                timerFinished = false;
                EventTimer.Start();
                while (SerialBufPointers[0] < 1 && timerFinished == false)
                    Application.DoEvents();
                EventTimer.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //failedCommunication = True
                return X8TYPE.X8TYPE_UNKNOWN;
            }

            Byte x8Type = SerialBuffer[0];

            timeDelayMS(10);
            return (X8TYPE)x8Type;
        }
        private void getNumSavedMeasurements(ref UInt16 numSaved_,ref UInt16 numUnused_,ref UInt16 numCleared_)
        {
            //CMDGetNumSavedMeasurements = 227
            Byte[] sendString = new Byte[3];
            sendString[0] = 1;
            sendString[1] = 227;  // CMDGetNumSavedMeasurements
            sendString[2] = 0xFF;

            // clear the serial buffer
            EventTimer.Start();
            timeDelayMS(10);
            EventTimer.Stop();

            SerialBufPointers[0] = 0;
            SerialBufPointers[1] = 0;

            try
            {
                commPort.Write(sendString, 0, 3);
                timerWait = 2000;
                timerFinished = false;
                EventTimer.Start();
                while (SerialBufPointers[0] < 6 && timerFinished == false)
                    Application.DoEvents();
                EventTimer.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //failedCommunication = True
                return;
            }

            Byte[] byteArr = new Byte[2];
            byteArr[0] = SerialBuffer[0];
            byteArr[1] = SerialBuffer[1];
            UInt16 numSaved = System.BitConverter.ToUInt16(byteArr, 0);
            byteArr[0] = SerialBuffer[2];
            byteArr[1] = SerialBuffer[3];
            UInt16 numUnused = System.BitConverter.ToUInt16(byteArr, 0);
            byteArr[0] = SerialBuffer[4];
            byteArr[1] = SerialBuffer[5];
            UInt16 numCleared = System.BitConverter.ToUInt16(byteArr, 0);

            numSaved_ = numSaved;
            numUnused_ = numUnused;
            numCleared_ = numCleared;

            timeDelayMS(10);

            return;
        }

        private void GetNumSavedMeasBTN_Click(object sender, EventArgs e)
        {
            UInt16 numSaved = 0;
            UInt16 numUnused = 0;
            UInt16 numCleared = 0;
            getNumSavedMeasurements(ref numSaved,ref numUnused,ref numCleared);
            timeDelayMS(100);
            getNumSavedMeasurements(ref numSaved, ref numUnused, ref numCleared);

            string msg = "Num saved measurements: " + numSaved + System.Environment.NewLine + "Num unused blocks: " + numUnused +
                           System.Environment.NewLine + "Num cleared blocks: " + numCleared;
            MessageBox.Show(msg);
        }

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        private void displaySavedMeasurementList(UInt16 startIndex_, Byte num_, Byte measType_)
        {
            //CMDGetNextNSavedMeasurements = 228
            // num_ maxes out at 10

            Byte[] versionBytes = BitConverter.GetBytes(startIndex_);

            Byte[] sendString = new Byte[7];
            sendString[0] = 1;
            sendString[1] = 228;  // CMDGetNextNSavedMeasurements
            sendString[2] = versionBytes[0];
            sendString[3] = versionBytes[1];
            sendString[4] = num_;
            sendString[5] = measType_;
            sendString[6] = 0xFF;

            // clear the serial buffer
            EventTimer.Start();
            timeDelayMS(10);
            EventTimer.Stop();

            SerialBufPointers[0] = 0;
            SerialBufPointers[1] = 0;

            Byte measDataSize = 8;
            Byte expectedBytes = (Byte)(num_*measDataSize);

            try
            {
                commPort.Write(sendString, 0, 7);
                timerWait = 200000;
                timerFinished = false;
                EventTimer.Start();
                while (SerialBufPointers[0] < expectedBytes && timerFinished == false)
                    Application.DoEvents();
                EventTimer.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //failedCommunication = True
                return;
            }

            Byte[] byteArr = new Byte[4];
           
            for(Byte i = 0; i<num_; i++){
                byteArr[0] = SerialBuffer[i*measDataSize];
                byteArr[1] = SerialBuffer[i*measDataSize+1];
                byteArr[2] = SerialBuffer[i*measDataSize+2];
                byteArr[3] = SerialBuffer[i*measDataSize+3];
                UInt32 timeStamp = System.BitConverter.ToUInt32(byteArr, 0);
                Byte channel = SerialBuffer[i*measDataSize + 4];
                channel += 1;
                Byte measType = SerialBuffer[i * measDataSize + 5];
                byteArr[0] = SerialBuffer[i * measDataSize + 6];
                byteArr[1] = SerialBuffer[i * measDataSize + 7];
                UInt16 blockLoc = System.BitConverter.ToUInt16(byteArr, 0);
                System.DateTime dtDateTime = UnixTimeStampToDateTime((double)timeStamp);
                string monthString = dtDateTime.Month.ToString("D2");
                string dayString = dtDateTime.Day.ToString("D2");
                string yearString = dtDateTime.Year.ToString("D2");
                string measTypeString = measType.ToString("X2");

                string destinations = "MachineDossier";
                if (measType == 3)
                {
                    destinations = "MachineDossier/EMonitor";
                    destIncludesMD.Add(true);
                    destIncludesEMonitor.Add(true);
                }
                else if (measType == 2)
                {
                    destinations = "EMonitor";
                    destIncludesMD.Add(false);
                    destIncludesEMonitor.Add(true);
                }
                else if (measType == 1)
                {
                    destinations = "MachineDossier";
                    destIncludesMD.Add(true);
                    destIncludesEMonitor.Add(false);
                }

                string measString = "(" + (startIndex_ + i).ToString("D3") + ")   " + monthString +"_" + dayString + "_" + yearString + 
                                 "    " + dtDateTime.Hour.ToString("D2") + ":" + dtDateTime.Minute.ToString("D2") +":" +
                                 dtDateTime.Second.ToString("D2") + "    Channel: " + channel.ToString("D2") + 
                                 "  Destinations: " + destinations + "  BlockNumber: " + blockLoc.ToString("D4");

                measurement_clb.Items.Add(measString, CheckState.Unchecked);
                blockLocList.Add(blockLoc);
            }

            timeDelayMS(10);

            return;
        }

        private void DisplaySavedMeasBTN_Click(object sender, EventArgs e)
        {
            UInt16 numSaved = 0;
            UInt16 numUnused = 0;
            UInt16 numCleared = 0;
            Byte measType = 0xff;
            Byte bunchSize = 10;
            getNumSavedMeasurements(ref numSaved, ref numUnused, ref numCleared);
            UInt16 numReads = (UInt16)(numSaved / (UInt16)bunchSize);

            measurement_clb.Items.Clear();

            UInt16 startIndex = 0;
            for (UInt16 i=0; i<numReads; i++)
            {
                startIndex = (UInt16)( i * bunchSize);
                displaySavedMeasurementList(startIndex, bunchSize, measType);
            }
            Byte remainder = (Byte)(numSaved % bunchSize);
            startIndex = (UInt16)(numReads * bunchSize);
            if (remainder != 0)
            { 
                displaySavedMeasurementList(startIndex, remainder, measType);
            }

            measurement_clb.Show();
            
        }

        private void SelectAllBTN_Click(object sender, EventArgs e)
        {
            // Select all items in measurement_clb
            for (int i = 0; i < measurement_clb.Items.Count; i++)
                measurement_clb.SetItemCheckState(i, CheckState.Checked);
        }

        private void ClearAllBTN_Click(object sender, EventArgs e)
        {
            // Clear all items in measurement_clb
            for (int i = 0; i < measurement_clb.Items.Count; i++)
                measurement_clb.SetItemCheckState(i, CheckState.Unchecked);
        }

        private void SelectAllBetweenCheckedBTN_Click(object sender, EventArgs e)
        {
            // iterate on all items and find the min checked and the max checked and select all in between
            int minCheckedIndex = 100000;
            int maxCheckedIndex = 0;

            for (int i = 0; i < measurement_clb.Items.Count; i++){
                if (measurement_clb.GetItemChecked(i))
                {
                    if (i > maxCheckedIndex)
                        maxCheckedIndex = i;
                    if (i < minCheckedIndex)
                        minCheckedIndex = i;
                }
            }

            for (int i = minCheckedIndex+1; i < maxCheckedIndex; i++)
                measurement_clb.SetItemCheckState(i, CheckState.Checked);
        }
        
        private Byte loadMeasurementByBlockLoc(UInt16 loc_)
        {
            // CMDLoadMeasurementByBlockLoc  = 230,
            Byte[] locBytes = BitConverter.GetBytes(loc_);
            Byte[] sendString = new Byte[5];

            sendString[0] = 1;
            sendString[1] = 230;  // CMDLoadMeasurementByIndex
            sendString[2] = locBytes[0];
            sendString[3] = locBytes[1];
            sendString[4] = 0xFF;

            // clear the serial buffer
            EventTimer.Start();
            timeDelayMS(10);
            EventTimer.Stop();

            SerialBufPointers[0] = 0;
            SerialBufPointers[1] = 0;

            try
            {
                commPort.Write(sendString, 0, 5);
                timerWait = 1000;
                timerFinished = false;
                EventTimer.Start();
                while (SerialBufPointers[0] < 1 && timerFinished == false)
                    Application.DoEvents();
                EventTimer.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //failedCommunication = True
                return 0;
            }

            Byte loadSucceeded = SerialBuffer[0];

            timeDelayMS(100);

            return loadSucceeded;

        }

        private Byte getOveralls()
        {
            //  CMDGetSavedMeasurementOveralls = 231

            Byte[] sendString = new Byte[3];
            sendString[0] = 1;
            sendString[1] = 231;  // CMDGetSavedMeasurementOveralls
            sendString[2] = 0xFF;

            // clear the serial buffer
            EventTimer.Start();
            timeDelayMS(100);
            EventTimer.Stop();

            SerialBufPointers[0] = 0;
            SerialBufPointers[1] = 0;

            Byte expectedBytes = 76;

            try
            {
                commPort.Write(sendString, 0, 3);
                timerWait = 10000;
                timerFinished = false;
                EventTimer.Start();
                while (SerialBufPointers[0] < expectedBytes && timerFinished == false)
                    Application.DoEvents();
                EventTimer.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //failedCommunication = True
                return 0;
            }

            for(int i=0; i<NUM_OVERALLS; ++i)
            {
                Byte[] byteArr = new Byte[4];
                byteArr[0] = SerialBuffer[i * sizeof(float) + 0];
                byteArr[1] = SerialBuffer[i * sizeof(float) + 1];
                byteArr[2] = SerialBuffer[i * sizeof(float) + 2];
                byteArr[3] = SerialBuffer[i * sizeof(float) + 3];
                overalls[i] = System.BitConverter.ToSingle(byteArr, 0);
            }

            timeDelayMS(10);

            Byte status = 1;
            if (timerFinished)
            {
                OverallsTimeoutChkBox.Checked = true;
                status = 0;
            }
            else
                OverallsTimeoutChkBox.Checked = false;

            return status;

        }

        private Byte getMiscellaneousMeasData()
        {
            //  CMDGetSavedMeasurementMisc = 232

            Byte[] sendString = new Byte[3];
            sendString[0] = 1;
            sendString[1] = 232;  // CMDGetSavedMeasurementMisc
            sendString[2] = 0xFF;

            // clear the serial buffer
            EventTimer.Start();
            timeDelayMS(10);
            EventTimer.Stop();

            SerialBufPointers[0] = 0;
            SerialBufPointers[1] = 0;

            Byte expectedBytes = 49;

            try
            {
                commPort.Write(sendString, 0, 3);
                timerWait = 1500;
                timerFinished = false;
                EventTimer.Start();
                while (SerialBufPointers[0] < expectedBytes && timerFinished == false)
                    Application.DoEvents();
                EventTimer.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //failedCommunication = True
                return 0;
            }

            Byte[] byteArr = new Byte[4];

            byteArr[0] = SerialBuffer[0];
            byteArr[1] = SerialBuffer[1];
            byteArr[2] = SerialBuffer[2];
            byteArr[3] = SerialBuffer[3];
            appVer = System.BitConverter.ToUInt32(byteArr, 0);

            dType = SerialBuffer[4];

            byteArr[0] = SerialBuffer[5];
            byteArr[1] = SerialBuffer[6];
            byteArr[2] = SerialBuffer[7];
            byteArr[3] = SerialBuffer[8];
            measDataLength = System.BitConverter.ToUInt32(byteArr, 0);
  
            byteArr[0] = SerialBuffer[9];
            byteArr[1] = SerialBuffer[10];
            byteArr[2] = SerialBuffer[11];
            byteArr[3] = SerialBuffer[12];
            nextSampleTime = System.BitConverter.ToUInt32(byteArr, 0);

            byteArr[0] = SerialBuffer[13];
            byteArr[1] = SerialBuffer[14];
            byteArr[2] = SerialBuffer[15];
            byteArr[3] = SerialBuffer[16];
            timeStampS = System.BitConverter.ToUInt32(byteArr, 0);

            byteArr[0] = SerialBuffer[17];
            byteArr[1] = SerialBuffer[18];
            byteArr[2] = SerialBuffer[19];
            byteArr[3] = SerialBuffer[20];
            inputRMS = System.BitConverter.ToSingle(byteArr, 0);

            byteArr[0] = SerialBuffer[21];
            byteArr[1] = SerialBuffer[22];
            byteArr[2] = SerialBuffer[23];
            byteArr[3] = SerialBuffer[24];
            maxFrequency = System.BitConverter.ToSingle(byteArr, 0);

            channelTested = SerialBuffer[25]; // 0-7
            channelTested++; // 1-8
            spectralBandLimitError = SerialBuffer[26];

            byteArr[0] = SerialBuffer[27];
            byteArr[1] = SerialBuffer[28];
            byteArr[2] = SerialBuffer[29];
            byteArr[3] = SerialBuffer[30];
            spectrumLength = System.BitConverter.ToUInt32(byteArr, 0);

            byteArr[0] = SerialBuffer[31];
            byteArr[1] = SerialBuffer[32];
            byteArr[2] = SerialBuffer[33];
            byteArr[3] = SerialBuffer[34];
            sampling_frequency = System.BitConverter.ToUInt32(byteArr, 0);

            byteArr[0] = SerialBuffer[35];
            byteArr[1] = SerialBuffer[36];
            byteArr[2] = SerialBuffer[37];
            byteArr[3] = SerialBuffer[38];
            maxBinValue = System.BitConverter.ToSingle(byteArr, 0);

            byteArr[0] = SerialBuffer[39];
            byteArr[1] = SerialBuffer[40];
            byteArr[2] = SerialBuffer[41];
            byteArr[3] = SerialBuffer[42];
            maxBinIndex = System.BitConverter.ToUInt32(byteArr, 0);

            byteArr[0] = SerialBuffer[43];
            byteArr[1] = SerialBuffer[44];
            byteArr[2] = SerialBuffer[45];
            byteArr[3] = SerialBuffer[46];
            waveformLength = System.BitConverter.ToUInt32(byteArr, 0);

            destinations = SerialBuffer[47];

            transducerType = (MDTransducerTypes)SerialBuffer[48];



            timeDelayMS(10);

            Byte status = 1;
            if (timerFinished)
                status = 0;
            return status;

        }

        private void requestSpectrumDataByXModem()
        {
            //  CMDGetSpectrumData         = 233

            Byte[] sendString = new Byte[3];
            sendString[0] = 1;
            sendString[1] = 233;  // CMDGetSpectrumDataByXModem
            sendString[2] = 0xFF;

            // clear the serial buffer
            EventTimer.Start();
            timeDelayMS(100);
            EventTimer.Stop();

            SerialBufPointers[0] = 0;
            SerialBufPointers[1] = 0;

            Byte expectedBytes = 1;

            try
            {
                commPort.Write(sendString, 0, 3);
                timerWait = 500;
                timerFinished = false;
                EventTimer.Start();
                while (SerialBufPointers[0] < expectedBytes && timerFinished == false)
                    Application.DoEvents();
                EventTimer.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //failedCommunication = True
                return;
            }
           
            timeDelayMS(10);

            return ;

        }

        private void CreateSpectrumChart()
        {
            var series = new Series("Spectrum");

            // Frist parameter is X-Axis and Second is Collection of Y- Axis
            //series.Points.DataBindXY(new[] { 2001, 2002, 2003, 2004 }, new[] { 100, 200, 90, 150 });
            for (UInt16 i = 0; i < spectrumLength; ++i)
            {
                series.Points.AddXY((double)i, (double)spectrumBuffer[i]);
            }

            SpectrumChart.Series.Clear();

            series.ChartType = SeriesChartType.Line;
            SpectrumChart.Titles.Clear();
            SpectrumChart.Titles.Add("Spectrum for channel " + channelTested.ToString());

            SpectrumChart.Series.Add(series);

        }

        private void transferSpectrumBufferBytesToSpectrumBuffer()
        {
            Byte[] byteArr = new Byte[4];

            for (UInt16 i = 0; i < spectrumLength; ++i) {
                byteArr[0] = SpectrumBufferBytes[i * 4 + 0];
                byteArr[1] = SpectrumBufferBytes[i * 4 + 1];
                byteArr[2] = SpectrumBufferBytes[i * 4 + 2];
                byteArr[3] = SpectrumBufferBytes[i * 4 + 3];
                spectrumBuffer[i] = System.BitConverter.ToSingle(byteArr, 0);
            }
        }

        private void getSpectrumDataByXModem()
        {
            requestSpectrumDataByXModem();
            // Instantiate XModemCommunicator.

            var xmodem = new XModemCommunicator();

            // Attach port.
            xmodem.Port = commPort;
            xmodem.Receive();

            //Array.Copy(SpectrumBufferBytes, xmodem.Data.ToArray(), measDataLength);
            if (measDataLength > (maxLOR * sizeof(float)))
                measDataLength = maxLOR * sizeof(float);

            Array.Copy(xmodem.Data.ToArray(), SpectrumBufferBytes, measDataLength);

            transferSpectrumBufferBytesToSpectrumBuffer();
            
            //MessageBox.Show("Spectrum[0]: " + spectrumBuffer[0].ToString() + "  spectrumBuffer[1]: " + spectrumBuffer[1].ToString());

            CreateSpectrumChart();
        }

        private void requestWaveformData()
        {
            //  CMDGetWaveformDataByXModem         = 234

            Byte[] sendString = new Byte[3];
            sendString[0] = 1;
            sendString[1] = 234;  // CMDGetWaveformDataByXModem
            sendString[2] = 0xFF;

            // clear the serial buffer
            EventTimer.Start();
            timeDelayMS(500);
            EventTimer.Stop();

            SerialBufPointers[0] = 0;
            SerialBufPointers[1] = 0;

            Byte expectedBytes = 1;

            try
            {
                commPort.Write(sendString, 0, 3);
                timerWait = 1000;
                timerFinished = false;
                EventTimer.Start();
                while (SerialBufPointers[0] < expectedBytes && timerFinished == false)
                    Application.DoEvents();
                EventTimer.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //failedCommunication = True
                return;
            }

            timeDelayMS(10);

            return;

        }

        private void CreateWaveformChart()
        {
            var series = new Series("Waveform");

            // Frist parameter is X-Axis and Second is Collection of Y- Axis
            //series.Points.DataBindXY(new[] { 2001, 2002, 2003, 2004 }, new[] { 100, 200, 90, 150 });
            for (UInt16 i = 0; i < waveformLength; ++i)
            {
                series.Points.AddXY((double)i, (double)WaveformBuffer[i]);
            }

            WaveformChart.Series.Clear();

            series.ChartType = SeriesChartType.Line;
            WaveformChart.Titles.Clear();
            WaveformChart.Titles.Add("Waveform for channel " + channelTested.ToString());

            WaveformChart.Series.Add(series);

        }

        private void transferWaveformBufferBytesToWaveformBuffer()
        {
            Byte[] byteArr = new Byte[4];

            for (UInt16 i = 0; i < waveformLength; ++i)
            {
                byteArr[0] = WaveformBufferBytes[i * 4 + 0];
                byteArr[1] = WaveformBufferBytes[i * 4 + 1];
                byteArr[2] = WaveformBufferBytes[i * 4 + 2];
                byteArr[3] = WaveformBufferBytes[i * 4 + 3];
                WaveformBuffer[i] = System.BitConverter.ToSingle(byteArr, 0);
            }
        }

        private void getWaveformDataByXModem()
        {
            requestWaveformData();
            // Instantiate XModemCommunicator.

            var xmodemWF = new XModemCommunicator();

            // Attach port.
            xmodemWF.Port = commPort;
            xmodemWF.Receive();

            if (waveformLength > MAX_WAVEFORM_LEN)
                waveformLength = MAX_WAVEFORM_LEN;
            Array.Copy(xmodemWF.Data.ToArray(), WaveformBufferBytes, waveformLength * sizeof(float));
            transferWaveformBufferBytesToWaveformBuffer();

            //MessageBox.Show("Spectrum[0]: " + spectrumBuffer[0].ToString() + "  spectrumBuffer[1]: " + spectrumBuffer[1].ToString());

            CreateWaveformChart();
        }

        private void SaveToPCBTN_Click(object sender, EventArgs e)
        {
            // Save Selected Measurements to Disk
            // Iterate on measurement_clb items
            PrimaryOverall_TB.Text = string.Empty;
            SecondaryOverall_TB.Text = string.Empty;

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "MDE File|*.mde";
            saveFileDialog1.Title = "Save measurements to a .mde file";
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.  
            if (saveFileDialog1.FileName != "")
            {
                // Saves the Image via a FileStream created by the OpenFile method.  
                System.IO.FileStream fs =
                   (System.IO.FileStream)saveFileDialog1.OpenFile();
                // NOTE that the FilterIndex property is one-based.  
                switch (saveFileDialog1.FilterIndex)
                {
                    case 1:
                        //this.exportButton.Image.Save(fs,
                        //   System.Drawing.Imaging.ImageFormat.Jpeg);

                        break;
                }
                if (fs.CanWrite)
                {
                    using (var writer = new StreamWriter(fs))
                    {
                        Application.UseWaitCursor = true;
                        UInt16 numSelected = 0;
                        for (int i = 0; i < measurement_clb.Items.Count; i++)
                        {
                            if (measurement_clb.GetItemChecked(i))
                                ++numSelected;
                        }
                        string debugStr = "Number of selected measurements: " + numSelected.ToString();
                        Debug_ListBox.Items.Add(debugStr);
                        Debug_ListBox.SelectedIndex = Debug_ListBox.Items.Count - 1;
                        Debug_ListBox.SelectedIndex = -1;
                        UInt16 selectIndex = 0;
                        for (int i = 0; i < measurement_clb.Items.Count; i++)
                        {
                            if (measurement_clb.GetItemChecked(i))
                            {

                                debugStr = "Collecting measurement data for selected measurement " + selectIndex.ToString();
                                Debug_ListBox.Items.Add(debugStr);
                                Debug_ListBox.SelectedIndex = Debug_ListBox.Items.Count - 1;
                                Debug_ListBox.SelectedIndex = -1;

                                UInt16 blockLoc = blockLocList[i];
                                // send command to device to load this measurement
                                if (loadMeasurementByBlockLoc((UInt16)blockLoc) == 0)
                                    continue;

                                // get measurement overalls
                                //getOveralls(); // for some reason we have to do a throw-away call to getOveralls()
                                if (getOveralls() == 0)
                                    continue;
                                PrimaryOverall_TB.Text = overalls[0].ToString();
                                SecondaryOverall_TB.Text = overalls[1].ToString();

                                // get measurement miscellaneous
                                if (getMiscellaneousMeasData() == 0)
                                    continue;

                                if (InclSpectrumChkBox.Checked)
                                {
                                    // get spectrum data
                                    debugStr = "Collecting spectrum data for selected measurement " + selectIndex.ToString();
                                    Debug_ListBox.Items.Add(debugStr);
                                    Debug_ListBox.SelectedIndex = Debug_ListBox.Items.Count - 1;
                                    Debug_ListBox.SelectedIndex = -1;
                                    //getSpectrumDataByXModem();
                                    getSpectrumByLargeBlockTransfer();
                                }

                                if (InclWaveformChkBox.Checked)
                                {
                                    // get waveform data
                                    debugStr = "Collecting waveform data for selected measurement " + selectIndex.ToString();
                                    Debug_ListBox.Items.Add(debugStr);
                                    Debug_ListBox.SelectedIndex = Debug_ListBox.Items.Count - 1;
                                    Debug_ListBox.SelectedIndex = -1;
                                    //getWaveformDataByXModem();
                                    getWaveformByLargeBlockTransfer();
                                }
                                //waveformLength = spectrumLength = 0;

                                debugStr = "Writing measurement data for selected measurement " + selectIndex.ToString();
                                Debug_ListBox.Items.Add(debugStr);
                                Debug_ListBox.SelectedIndex = Debug_ListBox.Items.Count - 1;
                                Debug_ListBox.SelectedIndex = -1;
                                writeMeasurementData(writer);
                                ++selectIndex;

                                timeDelayMS(1000);
                            }
                        }
                        debugStr = "Done writing measurements";
                        Debug_ListBox.Items.Add(debugStr);
                        Debug_ListBox.SelectedIndex = Debug_ListBox.Items.Count - 1;
                        Debug_ListBox.SelectedIndex = -1;
                        Application.UseWaitCursor = false;
                    }
                }

                fs.Close();
            }
        }

        private void GetX8TypeBTN_Click(object sender, EventArgs e)
        {
            deviceIsX8II = false;
            X8TYPE x8Type = checkX8Type();
            if (x8Type == X8TYPE.X8TYPE_X8II)
                deviceIsX8II = true;

            if (deviceIsX8II)
                MessageBox.Show("X8 Type: X8II");
            else
                MessageBox.Show("X8 Type: X8");

        }

        private void GetDeviceIsPaused()
        {
            //  CMDGetDeviceIsPaused        = 229
            Byte[] sendString = new Byte[3];
            sendString[0] = 1;
            sendString[1] = 229;  // CMDGetDeviceIsPaused
            sendString[2] = 0xFF;

            // clear the serial buffer
            EventTimer.Start();
            timeDelayMS(10);
            EventTimer.Stop();

            SerialBufPointers[0] = 0;
            SerialBufPointers[1] = 0;

            try
            {
                commPort.Write(sendString, 0, 3);
                timerWait = 30000;
                timerFinished = false;
                EventTimer.Start();
                while (SerialBufPointers[0] < 1 && timerFinished == false)
                    Application.DoEvents();
                EventTimer.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //failedCommunication = True
                return;
            }
            timeDelayMS(100);
            deviceIsPaused = SerialBuffer[0];

            timeDelayMS(10);

        }

        private void GetDeviceIsPausedBTN_Click(object sender, EventArgs e)
        {
            GetDeviceIsPaused();
            timeDelayMS(1000);
            if (deviceIsPaused==1)
                MessageBox.Show("Device is paused");
            else
                MessageBox.Show("Device is not paused");
        }

        private void writeOverallsAndMisc(StreamWriter writer)
        {
            PrimaryOverall_TB.Text = overalls[0].ToString();
            SecondaryOverall_TB.Text = overalls[1].ToString();

            string msg = "OverallA:      " + overalls[0] + System.Environment.NewLine +
                                    "OverallB:      " + overalls[1] + System.Environment.NewLine +
                                    "OverallC:      " + overalls[2] + System.Environment.NewLine +
                                    "OverallD:      " + overalls[3] + System.Environment.NewLine +
                                    "OverallE:      " + overalls[4] + System.Environment.NewLine +
                                    "DCV:           " + overalls[5] + System.Environment.NewLine +
                                    "Speed:         " + overalls[6] + System.Environment.NewLine +
                                    "OverallH:      " + overalls[7] + System.Environment.NewLine +
                                    "OverallI:      " + overalls[8] + System.Environment.NewLine +
                                    "OverallJ:      " + overalls[9] + System.Environment.NewLine +
                                    "OverallK:      " + overalls[10] + System.Environment.NewLine +
                                    "SpectralBandA: " + overalls[11] + System.Environment.NewLine +
                                    "SpectralBandB: " + overalls[12] + System.Environment.NewLine +
                                    "SpectralBandC: " + overalls[13] + System.Environment.NewLine +
                                    "SpectralBandD: " + overalls[14] + System.Environment.NewLine +
                                    "SpectralBandE: " + overalls[15] + System.Environment.NewLine +
                                    "SpectralBandF: " + overalls[16] + System.Environment.NewLine +
                                    "Accel Bias   : " + overalls[17] + System.Environment.NewLine +
                                    "Proximity Gap: " + overalls[18] + System.Environment.NewLine;

            //MessageBox.Show(msg);
            writer.Write(msg);


            msg = "AppVer: " + appVer + System.Environment.NewLine + "dType: " + dType + System.Environment.NewLine +
                "measDataLength: " + measDataLength + System.Environment.NewLine + "nextSampleTime: " + nextSampleTime + System.Environment.NewLine +
                "timeStampS: " + timeStampS + System.Environment.NewLine + "inputRMS: " + inputRMS + System.Environment.NewLine +
                "maxFrequency: " + maxFrequency + System.Environment.NewLine + "channelTested: " + channelTested + System.Environment.NewLine +
                "spectralBandLimitError: " + spectralBandLimitError + System.Environment.NewLine + "spectrumLength: " + spectrumLength + System.Environment.NewLine +
                "sampling_frequency: " + sampling_frequency + System.Environment.NewLine + "maxBinValue: " + maxBinValue + System.Environment.NewLine +
                "maxBinIndex: " + maxBinIndex + System.Environment.NewLine + "waveformLength: " + waveformLength + System.Environment.NewLine +
                "destinations: " + destinations + System.Environment.NewLine;

            //MessageBox.Show(msg);
            writer.Write(msg);
        }

        private void writeSpectrum(StreamWriter writer)
        {
            string msg = "Spectrum: ";
            UInt16 colCnt = 0;
            for (UInt16 i = 0; i < spectrumLength; ++i)
            {
                if (i < spectrumLength - 1)
                    msg = msg + spectrumBuffer[i].ToString() + ',';
                else
                    msg = msg + spectrumBuffer[i].ToString() + ';';
                colCnt += (UInt16)(spectrumBuffer[i].ToString().Length + 1);
                if (colCnt > 80)
                {
                    msg = msg + System.Environment.NewLine;
                    colCnt = 0;
                }
            }
            writer.Write(msg);
            writer.Write(System.Environment.NewLine);
        }

        private void writeWaveform(StreamWriter writer)
        {
            string msg = "Waveform: ";
            UInt16 colCnt = 0;
            for (UInt16 i = 0; i < waveformLength; ++i)
            {
                if (i < waveformLength - 1)
                    msg = msg + WaveformBuffer[i].ToString() + ',';
                else
                    msg = msg + WaveformBuffer[i].ToString() + ';';
                colCnt += (UInt16)(WaveformBuffer[i].ToString().Length + 1);
                if (colCnt > 80)
                {
                    msg = msg + System.Environment.NewLine;
                    colCnt = 0;
                }
            }
            writer.Write(msg);
            writer.Write(System.Environment.NewLine);
        }

        private void writeMachineDossierMeasurementData(StreamWriter writer_)
        {
            DateTime now = DateTime.Now;
            string time = now.ToString("T");
            string tmpStr = "MACHINE DOSSIER";
            writer_.Write(tmpStr);
            writer_.Write(System.Environment.NewLine);
            tmpStr = "exportDate, " + time;
            writer_.Write(tmpStr);
            writer_.Write(System.Environment.NewLine);
            Byte exportFormatVersion = 1; // hard coded
            writer_.Write("exportFormatVersion," + exportFormatVersion.ToString());
            writer_.Write(System.Environment.NewLine);
            string mdVersion = "2.8.3";
            writer_.Write("machineDossierVersion," + mdVersion);
            writer_.Write(System.Environment.NewLine);
            UInt32 startNodeId = 1;  // can't get this from the X8
            writer_.Write("startNodeId," + startNodeId.ToString());
            writer_.Write(System.Environment.NewLine);
            string startNodeName = "Chan " + channelTested;
            writer_.Write("startNodeName," + startNodeName);
            writer_.Write(System.Environment.NewLine);

        }

        private void writeMeasurementData(StreamWriter writer)
        {

            DateTime now = DateTime.Now;
            string time = now.ToString("T");
            string tmpStr = "# Measurement data written " + time;
            writer.Write(tmpStr);
            writer.Write(System.Environment.NewLine);

            writeOverallsAndMisc(writer);

            if (InclSpectrumChkBox.Checked)
                writeSpectrum(writer);

            if (InclWaveformChkBox.Checked)
                writeWaveform(writer);
        }

        private void JustLoadBTN_Click(object sender, EventArgs e)
        {
            // Save Selected Measurements to Disk
            // Iterate on measurement_clb items
            PrimaryOverall_TB.Text = string.Empty;
            SecondaryOverall_TB.Text = string.Empty;

            
            Application.UseWaitCursor = true;
            UInt16 numSelected = 0;
            for (int i = 0; i < measurement_clb.Items.Count; i++)
            {
                if (measurement_clb.GetItemChecked(i))
                    ++numSelected;
            }
            string debugStr = "Number of selected measurements: " + numSelected.ToString();
            Debug_ListBox.Items.Add(debugStr);
            Debug_ListBox.SelectedIndex = Debug_ListBox.Items.Count - 1;
            Debug_ListBox.SelectedIndex = -1;
            UInt16 selectIndex = 0;
            for (int i = 0; i < measurement_clb.Items.Count; i++)
            {
                if (measurement_clb.GetItemChecked(i))
                {

                    debugStr = "Collecting measurement data for selected measurement " + selectIndex.ToString();
                    Debug_ListBox.Items.Add(debugStr);
                    Debug_ListBox.SelectedIndex = Debug_ListBox.Items.Count - 1;
                    Debug_ListBox.SelectedIndex = -1;

                    UInt16 blockLoc = blockLocList[i];
                    // send command to device to load this measurement
                    if (loadMeasurementByBlockLoc((UInt16)blockLoc) == 0)
                        continue;

                    // get measurement overalls
                    //getOveralls(); // for some reason we have to do a throw-away call to getOveralls()
                    if (getOveralls() == 0)
                        continue;

                   

                    // get measurement miscellaneous
                    if (getMiscellaneousMeasData() == 0)
                       continue;

                    // check the transducerType
                    if (transducerType == MDTransducerTypes.Accelerometer)
                    {
                        PrimaryOverall_LBL.Text = "Velocity Overall";
                        PrimaryOverall_TB.Text = overalls[0].ToString();
                        SecondaryOverall_LBL.Text = "Acceleration Overall";
                        SecondaryOverall_TB.Text = overalls[2].ToString();
                    }
                    else if (transducerType == MDTransducerTypes.ProximityProbe)
                    {
                        PrimaryOverall_LBL.Text = "Displacement Overall";
                        PrimaryOverall_TB.Text = overalls[8].ToString();
                        SecondaryOverall_LBL.Text = "N/A";
                        SecondaryOverall_TB.Text = String.Empty;
                    }


                    //spectrumLength = 3200;
                    //measDataLength = 12800;
                    if (InclSpectrumChkBox.Checked)
                    {
                        // get spectrum data
                        debugStr = "Collecting spectrum data for selected measurement " + selectIndex.ToString();
                        Debug_ListBox.Items.Add(debugStr);
                        Debug_ListBox.SelectedIndex = Debug_ListBox.Items.Count - 1;
                        Debug_ListBox.SelectedIndex = -1;
                        //getSpectrumDataByXModem();
                        getSpectrumByLargeBlockTransfer();
                    }

                    if (InclWaveformChkBox.Checked)
                    {
                        // get waveform data
                        debugStr = "Collecting waveform data for selected measurement " + selectIndex.ToString();
                        Debug_ListBox.Items.Add(debugStr);
                        Debug_ListBox.SelectedIndex = Debug_ListBox.Items.Count - 1;
                        Debug_ListBox.SelectedIndex = -1;
                        //getWaveformDataByXModem();
                        getWaveformByLargeBlockTransfer();
                    }
                    //waveformLength = spectrumLength = 0;
                    ++selectIndex;

                    timeDelayMS(1000);
                }
            }
            debugStr = "Done loading measurements";
            Debug_ListBox.Items.Add(debugStr);
            Debug_ListBox.SelectedIndex = Debug_ListBox.Items.Count - 1;
            Debug_ListBox.SelectedIndex = -1;
            Application.UseWaitCursor = false;

        }

        private void checkForSaveTypes(ref bool includesMD_, ref bool includesEMonitor_)
        {
            for (int i = 0; i < measurement_clb.Items.Count; i++)
            {
                if (measurement_clb.GetItemChecked(i))
                {
                    if (destIncludesMD[i])
                        includesMD_ = true;
                    if (destIncludesEMonitor[i])
                        includesEMonitor_ = true;
                }
            }

        }

        private void debugLog(string debugStr_)
        {
            Debug_ListBox.Items.Add(debugStr_);
            Debug_ListBox.SelectedIndex = Debug_ListBox.Items.Count - 1;
            Debug_ListBox.SelectedIndex = -1;
        }

        private void saveMDMeasurements()
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "CSV File|*.csv";
            saveFileDialog1.Title = "Save Machine Dossier measurements to a .csv file";
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.  
            if (saveFileDialog1.FileName != "")
            {
                // Saves the Image via a FileStream created by the OpenFile method.  
                System.IO.FileStream fs =
                   (System.IO.FileStream)saveFileDialog1.OpenFile();
                // NOTE that the FilterIndex property is one-based.  
                switch (saveFileDialog1.FilterIndex)
                {
                    case 1:
                        //this.exportButton.Image.Save(fs,
                        //   System.Drawing.Imaging.ImageFormat.Jpeg);

                        break;
                }
                if (fs.CanWrite)
                {
                    using (var writer = new StreamWriter(fs))
                    {
                        Application.UseWaitCursor = true;
                        UInt16 numSelected = 0;
                        for (int i = 0; i < measurement_clb.Items.Count; i++)
                        {
                            if (measurement_clb.GetItemChecked(i))
                                ++numSelected;
                        }
                        string debugStr = "Number of selected measurements: " + numSelected.ToString();
                        debugLog(debugStr);
                       
                        UInt16 selectIndex = 0;
                        for (int i = 0; i < measurement_clb.Items.Count; i++)
                        {
                            if (measurement_clb.GetItemChecked(i))
                            {

                                debugStr = "Collecting measurement data for selected measurement " + selectIndex.ToString();
                                Debug_ListBox.Items.Add(debugStr);
                                Debug_ListBox.SelectedIndex = Debug_ListBox.Items.Count - 1;
                                Debug_ListBox.SelectedIndex = -1;

                                UInt16 blockLoc = blockLocList[i];
                                // send command to device to load this measurement
                                if (loadMeasurementByBlockLoc((UInt16)blockLoc) == 0)
                                    continue;

                                // get measurement overalls
                                //getOveralls(); // for some reason we have to do a throw-away call to getOveralls()
                                if (getOveralls() == 0)
                                    continue;
                                PrimaryOverall_TB.Text = overalls[0].ToString();
                                SecondaryOverall_TB.Text = overalls[1].ToString();

                                // get measurement miscellaneous
                                if (getMiscellaneousMeasData() == 0)
                                    continue;

                                if (InclSpectrumChkBox.Checked)
                                {
                                    // get spectrum data
                                    debugStr = "Collecting spectrum data for selected measurement " + selectIndex.ToString();
                                    Debug_ListBox.Items.Add(debugStr);
                                    Debug_ListBox.SelectedIndex = Debug_ListBox.Items.Count - 1;
                                    Debug_ListBox.SelectedIndex = -1;
                                    //getSpectrumData();
                                    getSpectrumByLargeBlockTransfer();
                                }

                                if (InclWaveformChkBox.Checked)
                                {
                                    // get waveform data
                                    debugStr = "Collecting waveform data for selected measurement " + selectIndex.ToString();
                                    Debug_ListBox.Items.Add(debugStr);
                                    Debug_ListBox.SelectedIndex = Debug_ListBox.Items.Count - 1;
                                    Debug_ListBox.SelectedIndex = -1;
                                    //getWaveformDataByXModem();
                                    getWaveformByLargeBlockTransfer();
                                }
                                //waveformLength = spectrumLength = 0;

                                debugStr = "Writing MD measurement data for selected measurement " + selectIndex.ToString();
                                Debug_ListBox.Items.Add(debugStr);
                                Debug_ListBox.SelectedIndex = Debug_ListBox.Items.Count - 1;
                                Debug_ListBox.SelectedIndex = -1;
                                writeMachineDossierMeasurementData(writer);
                                ++selectIndex;

                                timeDelayMS(1000);
                            }
                        }
                        debugStr = "Done writing measurements";
                        Debug_ListBox.Items.Add(debugStr);
                        Debug_ListBox.SelectedIndex = Debug_ListBox.Items.Count - 1;
                        Debug_ListBox.SelectedIndex = -1;
                        Application.UseWaitCursor = false;
                    }
                }

                fs.Close();
            }
        }
        private void SaveMeasurementsBTN_Click(object sender, EventArgs e)
        {
            // Save Selected Measurements to Disk
            // Iterate on measurement_clb items
            PrimaryOverall_TB.Text = string.Empty;
            SecondaryOverall_TB.Text = string.Empty;

            // check for selected measurements to see if they are for MD, or EMonitor, or both
            // Prompt user MD filesave, and save all MD saved measurements, then prompt user for EMonitor file save, and save those measurements

            destIncludesMD.Add(true);
            destIncludesEMonitor.Add(false);

            bool includesMD = false;
            bool includesEMonitor = false;
            checkForSaveTypes(ref includesMD, ref includesEMonitor);

            if (includesMD)
            {
                saveMDMeasurements();
            }

            if (includesEMonitor)
            {

            }
        }

        UInt32 CRC_calcBytes2crc32(Byte[] byteStream, UInt32 byteStreamLength)
        {
            UInt32 crcCode = 0xFFFFFFFF;
            UInt32 CRC_32_POLYNOM = 0xEDB88320;

            //DIRECT CALCULATION
            //Byte one at a time
            for (UInt32 j = 0; j < byteStreamLength; j++){
                UInt32 tempByte = byteStream[j];
                UInt32 temp = (crcCode ^ (UInt32)tempByte) & 0xff;

                //Bits one at a time
                for (UInt32 i = 0; i < 8; i++){
                    if ((temp & 1) == 1)
                        temp = (temp >> 1) ^ CRC_32_POLYNOM;
                    else
                        temp = (temp >> 1);
                }                        //for bits
                crcCode = (crcCode >> 8) ^ temp;
            }                        //for bytes
                                     //Invert crcCode
            crcCode = crcCode ^ 0xFFFFFFFF;
            return crcCode; //Calculated CRC value
        } ///< CRC_calcBytes2crc32

        private bool checkForValidCRC()
        {
            UInt32 calcCRC = CRC_calcBytes2crc32(SerialBuffer, LARGE_BLOCK_PAYLOAD_SIZE+3);
            Byte[] byteArr = new Byte[4];
            byteArr[0] = SerialBuffer[LARGE_BLOCK_PAYLOAD_SIZE + 3];
            byteArr[1] = SerialBuffer[LARGE_BLOCK_PAYLOAD_SIZE + 4];
            byteArr[2] = SerialBuffer[LARGE_BLOCK_PAYLOAD_SIZE + 5];
            byteArr[3] = SerialBuffer[LARGE_BLOCK_PAYLOAD_SIZE + 6];
            UInt32 receivedCRC = System.BitConverter.ToUInt32(byteArr, 0);
            
            return (calcCRC==receivedCRC);
        }


        private bool getNextSpectrumBlockByLargeBlockTransfer(Byte blockNum_, UInt16 dataLen_)
        {
            // CMDGetNextSpectrumBlock = 236,
            Byte[] argBytes = BitConverter.GetBytes(dataLen_);
            Byte[] sendString = new Byte[6];

            sendString[0] = 1;
            sendString[1] = 236;  // CMDGetNextSpectrumBlock
            sendString[2] = blockNum_;
            sendString[3] = argBytes[0];
            sendString[4] = argBytes[1];
            sendString[5] = 0xFF;

            // clear the serial buffer
            EventTimer.Start();
            timeDelayMS(10);
            EventTimer.Stop();

            SerialBufPointers[0] = 0;
            SerialBufPointers[1] = 0;

            UInt16 expectedBytes = LARGE_BLOCK_PAYLOAD_SIZE + 7; // 10k + 3 leading bytes + 4 crc bytes at end

            receivingSpectrum = true;
            try
            {
                commPort.Write(sendString, 0, 6);
                timerWait = 10000;
                timerFinished = false;
                EventTimer.Start();
                while (SerialBufPointers[0] < expectedBytes && timerFinished == false)
                    Application.DoEvents();
                EventTimer.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //failedCommunication = True
                return false;
            }

            receivingSpectrum = false;

            if (SerialBuffer[0] != (Byte)ASCII_TABLE.ASCII_SOH  || 
               SerialBuffer[1] != (blockNum_ & 0x00FF) || 
               SerialBuffer[2] != (0xFF - (blockNum_ & 0x00FF))){
                return false;
            }

            if (!checkForValidCRC())
                return false;

            // add to SpectrumBufferBytes
            UInt16 startIndex = (UInt16)((UInt16)blockNum_ * LARGE_BLOCK_PAYLOAD_SIZE);
            for (UInt16 i=0; i< dataLen_; ++i)
                SpectrumBufferBytes[startIndex + i] = SerialBuffer[i + 3];

            timeDelayMS(10);

            return true;

        }

        private void getSpectrumByLargeBlockTransfer()
        {
            // calculate how many 10,000 byte blocks are in the current spectrum
            UInt16 spectrumDataSize = (UInt16)(spectrumLength * sizeof(float));
            Byte numCompleteBlocks = (Byte)(spectrumDataSize / LARGE_BLOCK_PAYLOAD_SIZE );
            UInt16 remainingDataSize = 0;
            if((spectrumDataSize % LARGE_BLOCK_PAYLOAD_SIZE) != 0)
                remainingDataSize = (UInt16)(spectrumDataSize % LARGE_BLOCK_PAYLOAD_SIZE);

            for(Byte blockNum = 0; blockNum < numCompleteBlocks; ++blockNum)
            {
                while(!getNextSpectrumBlockByLargeBlockTransfer(blockNum, LARGE_BLOCK_PAYLOAD_SIZE))
                {
                    timeDelayMS(1);
                }
                //debugLog("Block: " + blockNum.ToString() + " size: 10000");
            }

            if(remainingDataSize > 0)
            {
                while(!getNextSpectrumBlockByLargeBlockTransfer((Byte)numCompleteBlocks, remainingDataSize))
                {
                    timeDelayMS(1);
                }
                //debugLog("Block: " + numCompleteBlocks.ToString() + " size: " + remainingDataSize);
            }

            if (measDataLength > (maxLOR * sizeof(float)))
                measDataLength = maxLOR * sizeof(float);
            transferSpectrumBufferBytesToSpectrumBuffer();
            CreateSpectrumChart();
        }

        private bool getNextWaveformBlockByLargeBlockTransfer(Byte blockNum_, UInt16 dataLen_)
        {
            // CMDGetNextWaveformBlock = 237
            Byte[] argBytes = BitConverter.GetBytes(dataLen_);
            Byte[] sendString = new Byte[6];

            sendString[0] = 1;
            sendString[1] = 237;  // CMDGetNextWaveformBlock
            sendString[2] = blockNum_;
            sendString[3] = argBytes[0];
            sendString[4] = argBytes[1];
            sendString[5] = 0xFF;

            // clear the serial buffer
            EventTimer.Start();
            timeDelayMS(10);
            EventTimer.Stop();

            SerialBufPointers[0] = 0;
            SerialBufPointers[1] = 0;

            UInt16 expectedBytes = LARGE_BLOCK_PAYLOAD_SIZE + 7; // 10k + 3 leading bytes + 4 crc bytes at end

            receivingSpectrum = true;
            try
            {
                commPort.Write(sendString, 0, 6);
                timerWait = 10000;
                timerFinished = false;
                EventTimer.Start();
                while (SerialBufPointers[0] < expectedBytes && timerFinished == false)
                    Application.DoEvents();
                EventTimer.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //failedCommunication = True
                return false;
            }

            receivingSpectrum = false;

            if (SerialBuffer[0] != (Byte)ASCII_TABLE.ASCII_SOH ||
               SerialBuffer[1] != (blockNum_ & 0x00FF) ||
               SerialBuffer[2] != (0xFF - (blockNum_ & 0x00FF)))
            {
                return false;
            }

            if (!checkForValidCRC())
                return false;

            // add to SpectrumBufferBytes
            UInt16 startIndex = (UInt16)((UInt16)blockNum_ * LARGE_BLOCK_PAYLOAD_SIZE);
            for (UInt16 i = 0; i < dataLen_; ++i)
                WaveformBufferBytes[startIndex + i] = SerialBuffer[i + 3];

            timeDelayMS(10);

            return true;
        }

        private void getWaveformByLargeBlockTransfer()
        {
            // calculate how many 10,000 byte blocks are in the current waveform
            UInt16 waveformDataSize = (UInt16)(waveformLength * sizeof(float));
            Byte numCompleteBlocks = (Byte)(waveformDataSize / LARGE_BLOCK_PAYLOAD_SIZE);
            UInt16 remainingDataSize = 0;
            if ((waveformDataSize % LARGE_BLOCK_PAYLOAD_SIZE) != 0)
                remainingDataSize = (UInt16)(waveformDataSize % LARGE_BLOCK_PAYLOAD_SIZE);

            for (Byte blockNum = 0; blockNum < numCompleteBlocks; ++blockNum)
            {
                while(!getNextWaveformBlockByLargeBlockTransfer(blockNum, LARGE_BLOCK_PAYLOAD_SIZE))
                {
                    timeDelayMS(1);
                }
                //debugLog("Block: " + blockNum.ToString() + " size: 10000");
            }

            if (remainingDataSize > 0)
            {
                while(!getNextWaveformBlockByLargeBlockTransfer(numCompleteBlocks, remainingDataSize))
                {
                    timeDelayMS(1);
                }
                //debugLog("Block: " + numCompleteBlocks.ToString() + " size: " + remainingDataSize);
            }

            if (waveformLength > MAX_WAVEFORM_LEN)
                waveformLength = MAX_WAVEFORM_LEN;

            transferWaveformBufferBytesToWaveformBuffer();
            CreateWaveformChart();
        }

        private void OverallA_TB_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
